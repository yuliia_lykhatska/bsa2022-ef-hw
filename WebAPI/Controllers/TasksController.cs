﻿using Application;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Application.DTO;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
        public class TasksController : ControllerBase
        {
            private readonly TaskService service;
            public TasksController(TaskService _service)
            {
                service = _service;
            }
            [HttpPost]
            public ActionResult<TaskDTO> Create(TaskDTO Task)
            {
                try
                {
                    service.Create(Task);
                    return Created($"api/Tasks/{Task.Id}", Task);
                }
                catch (InvalidOperationException e)
                {
                    return Conflict(e.Message);
                }

            }

            [HttpGet]
            public ActionResult<List<TaskDTO>> Read()
            {
                return Ok(service.Read());
            }

            [HttpGet("{id}")]
            public ActionResult<TaskDTO> Read(int id)
            {
                try
                {
                    return Ok(service.Read(id));
                }
                catch (KeyNotFoundException e)
                {
                    return NotFound(e.Message);
                }
            }

            [HttpPut("{id}")]
            public ActionResult Update(TaskDTO Task)
            {
                try
                {
                    service.Update(Task);
                    return Ok();
                }
                catch (KeyNotFoundException e)
                {
                    return Conflict(e.Message);
                }
            }

            [HttpDelete("{id}")]
            public ActionResult Delete(int id)
            {
                try
                {
                    service.Delete(id);
                    return Ok();
                }
                catch (KeyNotFoundException e)
                {
                    return NotFound(e.Message);
                }
            }
        }
}
