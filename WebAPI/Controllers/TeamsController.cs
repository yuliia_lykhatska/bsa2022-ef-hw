﻿using Application;
using Application.DTO;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamService service;
        public TeamsController(TeamService _service)
        {
            service = _service;
        }
        [HttpPost]
        public ActionResult<TeamDTO> Create(TeamDTO Team)
        {
            try
            {
                service.Create(Team);
                return Created($"api/Teams/{Team.Id}", Team);
            }
            catch (InvalidOperationException e)
            {
                return Conflict(e.Message);
            }

        }

        [HttpGet]
        public ActionResult<List<TeamDTO>> Read()
        {
            return Ok(service.Read());
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDTO> Read(int id)
        {
            try
            {
                return Ok(service.Read(id));
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPut("{id}")]
        public ActionResult Update(TeamDTO Team)
        {
            try
            {
                service.Update(Team);
                return Ok();
            }
            catch (KeyNotFoundException e)
            {
                return Conflict(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                service.Delete(id);
                return Ok();
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
