﻿using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Application;
using Application.DTO;
using static Application.LinqHWService;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqHWController : ControllerBase
    {
        private readonly LinqHWService service;

        public LinqHWController(LinqHWService _service)
        {
            service = _service;
        }

        [HttpGet("1")]
        public ActionResult<Dictionary<Project, int>> TasksPerProject(int userId)
        {
            return Ok(service.CountTasksByEachProjectForUser(userId));
        }

        [HttpGet("2")]
        public ActionResult<TaskDTO> TasksWithLongNames(int userId)
        {
            return Ok(service.GetTasksWithNameLongerThan45ForUser(userId));
        }

        [HttpGet("3")]
        public ActionResult<TaskInfo> TasksFinishedIn2021(int userId)
        {
            return Ok(service.GetTasksFinishedIn2021ForUser(userId));
        }

        [HttpGet("4")]
        public ActionResult<TeamInfo> TeamsWithGrownMembers()
        {
            return Ok(service.GetTeamsWithMembersOlderThan10());
        }

        [HttpGet("5")]
        public ActionResult<List<(User, List<Task>)>> UsersWithTasks()
        {
            return Ok(service.GetUsersWithTasks());
        }
    }
}
