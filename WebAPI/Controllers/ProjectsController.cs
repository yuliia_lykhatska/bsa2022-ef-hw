﻿using Application;
using Application.DTO;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService service;
        public ProjectsController(ProjectService _service)
        {
            service = _service;
        }
        [HttpPost]
        public ActionResult<ProjectDTO> Create(ProjectDTO project)
        {
            try
            {
                service.Create(project);
                return Created($"api/Projects/{project.Id}", project);
            }
            catch (InvalidOperationException e)
            {
                return Conflict(e.Message);
            }
            
        }

        [HttpGet]
        public ActionResult<List<ProjectDTO>> Read()
        {
            return Ok(service.Read());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Read(int id)
        {
            try
            {
                return Ok(service.Read(id));
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPut("{id}")]
        public ActionResult Update(ProjectDTO project)
        {
            try
            {
                service.Update(project);
                return Ok();
            }
            catch (KeyNotFoundException e)
            {
                return Conflict(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                service.Delete(id);
                return Ok();
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
