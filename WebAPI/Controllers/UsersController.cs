﻿using Application;
using Application.DTO;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;


namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService service;
        public UsersController(UserService _service)
        {
            service = _service;
        }
        [HttpPost]
        public ActionResult<UserDTO> Create(UserDTO User)
        {
            try
            {
                service.Create(User);
                return Created($"api/Users/{User.Id}", User);
            }
            catch (InvalidOperationException e)
            {
                return Conflict(e.Message);
            }

        }

        [HttpGet]
        public ActionResult<List<UserDTO>> Read()
        {
            return Ok(service.Read());
        }

        [HttpGet("{id}")]
        public ActionResult<UserDTO> Read(int id)
        {
            try
            {
                return Ok(service.Read(id));
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPut("{id}")]
        public ActionResult Update(UserDTO User, int id)
        {
            try
            {
                service.Update(User, id);
                return Ok();
            }
            catch (KeyNotFoundException e)
            {
                return Conflict(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                service.Delete(id);
                return Ok();
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
