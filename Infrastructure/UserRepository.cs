﻿using Domain.Entities;
using Infrastructure;
using System.Collections.Generic;
using System.Linq;


namespace Infrastucture
{
    public class UserRepository : IRepository<User>
    {
        private readonly LinqDbContext context;

        public UserRepository(LinqDbContext _context)
        {
            context = _context;
        }
        public void Create(User entity)
        {
            context.Users.Add(entity);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var entity = Read(id);
            if (entity != null)
            {
                context.Users.Attach(entity);
                context.Users.Remove(entity);
                context.SaveChanges();
            }
        }

        public User Read(int id)
        {
            return context.Users.FirstOrDefault(User => User.Id == id);
        }

        public List<User> Read()
        {
            return context.Users.ToList();
        }

        public void Update(User entity)
        {
            var entityToUpdate = Read(entity.Id);
            foreach (var property in typeof(User).GetProperties())
            {
                var value = property.GetValue(entity);
                if( value != null)
                {
                    property.SetValue(entityToUpdate, value);
                }
            }
            context.SaveChanges();
        }
    }
}
