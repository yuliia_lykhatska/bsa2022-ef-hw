﻿using Bogus;
using Domain.Entities;
using System.Collections.Generic;


namespace Infrastucture
{
    public class Data
    {
        public List<Project> Projects { get; set; }
        public List<Task> Tasks { get; set; }
        public List<User> Users { get; set; }
        public List<Team> Teams { get; set; }
        public Data()
        {
            Projects = new List<Project>();
            Users = new List<User>();
            Teams = new List<Team>();
            Tasks = new List<Task>();
            GenerateUsers();
            GenerateTasks();
            GenerateTeams();
            GenerateProjects();
        }
        private void GenerateUsers()
        {
            var users = new Faker<User>()
                .RuleFor(u => u.Id, f => f.IndexFaker)
                .RuleFor(u => u.TeamId, f => f.Random.Int(0, 20))
                .RuleFor(u => u.FirstName, f => f.Random.Word())
                .RuleFor(u => u.LastName, f => f.Random.Word())
                .RuleFor(u => u.Email, f => f.Random.Word())
                .RuleFor(u => u.RegisteredAt, f => f.Date.Between(new System.DateTime(2000), System.DateTime.Now))
                .RuleFor(u => u.BirthDay, f => f.Date.Between(new System.DateTime(1940), new System.DateTime(2005)));

            for(int i = 0; i < 100; i++)
            {
                Users.Add(users.Generate());
            }
        }
        private void GenerateTasks()
        {
            var tasks = new Faker<Task>()
                .RuleFor(t => t.Id, f => f.IndexFaker)
                .RuleFor(t => t.ProjectId, f => f.Random.Int(0, 20))
                .RuleFor(t => t.PerformerId, f => f.Random.Int(0, 100))
                .RuleFor(t => t.Name, f => f.Random.Word())
                .RuleFor(t => t.Description, f => f.Random.Words(3))
                .RuleFor(t => t.State, f => f.Random.Int(1, 4))
                .RuleFor(t => t.CreatedAt, f => f.Date.Between(new System.DateTime(2000), System.DateTime.Now))
                .RuleFor(t => t.FinishedAt, f => f.Date.Between(new System.DateTime(2000), System.DateTime.Now));
            
            for(int i = 0; i < 300; i++)
            {
                Tasks.Add(tasks.Generate());
            }
        }
        private void GenerateTeams()
        {
            var teams = new Faker<Team>()
                .RuleFor(t => t.Id, f => f.IndexFaker)
                .RuleFor(t => t.Name, f => f.Random.Word())
                .RuleFor(t => t.CreatedAt, f => f.Date.Between(new System.DateTime(2000), System.DateTime.Now));
            
            for (int i = 0; i < 20; i++)
            {
               Teams.Add(teams.Generate());
            }
        }
        private void GenerateProjects()
        {
            var projects = new Faker<Project>()
                .RuleFor(p => p.Id, f => f.IndexFaker)
                .RuleFor(p => p.Name, f => f.Random.Word())
                .RuleFor(p => p.Description, f => f.Random.Words(3))
                .RuleFor(p => p.AuthorId, f => f.Random.Int(0, 20))
                .RuleFor(p => p.TeamId, f => f.Random.Int(0, 20))
                .RuleFor(p => p.CreatedAt, f => f.Date.Between(new System.DateTime(2000), System.DateTime.Now))
                .RuleFor(p => p.Deadline, f => f.Date.Between(new System.DateTime(2000), System.DateTime.Now));

            for (int i = 0; i < 20; i++)
            {
                Projects.Add(projects.Generate());
            }
        }
    }
}
