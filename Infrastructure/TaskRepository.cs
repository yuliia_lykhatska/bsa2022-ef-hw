﻿using Domain.Entities;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastucture
{
    public class TaskRepository : IRepository<Task>
    {
        private readonly LinqDbContext context;

        public TaskRepository(LinqDbContext _context)
        {
            context = _context;
        }

        public void Create(Task entity)
        {
            context.Tasks.Add(entity);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var entity = Read(id);
            if (entity != null)
            {
                context.Tasks.Attach(entity);
                context.Tasks.Remove(entity);
                context.SaveChanges();
            }
        }

        public Task Read(int id)
        {
            return context.Tasks.FirstOrDefault(Task => Task.Id == id);
        }

        public List<Task> Read()
        {
            return context.Tasks.ToList();
        }

        public void Update(Task entity)
        {
            var entityToUpdate = Read(entity.Id);
            foreach (var property in typeof(User).GetProperties())
            {
                var value = property.GetValue(entity);
                if (value != null)
                {
                    property.SetValue(entityToUpdate, value);
                }
            }
            context.SaveChanges();
        }
    }
}
