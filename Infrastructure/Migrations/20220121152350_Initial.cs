﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TeamId = table.Column<int>(type: "int", nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RegisteredAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BirthDay = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AuthorId = table.Column<int>(type: "int", nullable: false),
                    TeamId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Deadline = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Projects_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    PerformerId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    State = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FinishedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tasks_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Tasks_Users_PerformerId",
                        column: x => x.PerformerId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(1072, 6, 14, 0, 15, 12, 632, DateTimeKind.Unspecified).AddTicks(7248), "New Mexico" },
                    { 18, new DateTime(1669, 5, 5, 14, 45, 37, 382, DateTimeKind.Unspecified).AddTicks(5360), "Administrator" },
                    { 17, new DateTime(521, 6, 6, 18, 43, 48, 456, DateTimeKind.Unspecified).AddTicks(3632), "hack" },
                    { 16, new DateTime(1056, 11, 16, 3, 37, 23, 525, DateTimeKind.Unspecified).AddTicks(6912), "Concrete" },
                    { 15, new DateTime(1279, 11, 11, 8, 9, 2, 175, DateTimeKind.Unspecified).AddTicks(2080), "Investor" },
                    { 14, new DateTime(203, 2, 8, 23, 10, 52, 928, DateTimeKind.Unspecified).AddTicks(4576), "wireless" },
                    { 13, new DateTime(1762, 9, 8, 5, 2, 46, 335, DateTimeKind.Unspecified).AddTicks(1824), "Reunion" },
                    { 12, new DateTime(1785, 9, 29, 19, 9, 56, 887, DateTimeKind.Unspecified).AddTicks(4320), "grey" },
                    { 11, new DateTime(469, 8, 17, 20, 3, 26, 249, DateTimeKind.Unspecified).AddTicks(8768), "Generic Soft Hat" },
                    { 10, new DateTime(1516, 8, 5, 13, 19, 10, 757, DateTimeKind.Unspecified).AddTicks(6592), "Avenue" },
                    { 9, new DateTime(1705, 6, 19, 3, 9, 49, 393, DateTimeKind.Unspecified).AddTicks(7872), "Technician" },
                    { 8, new DateTime(620, 10, 26, 5, 20, 4, 579, DateTimeKind.Unspecified).AddTicks(8448), "whiteboard" },
                    { 7, new DateTime(1539, 2, 11, 17, 48, 7, 306, DateTimeKind.Unspecified).AddTicks(7088), "Tools, Tools & Toys" },
                    { 6, new DateTime(900, 1, 14, 2, 17, 48, 799, DateTimeKind.Unspecified).AddTicks(9760), "Clothing, Baby & Shoes" },
                    { 5, new DateTime(823, 8, 1, 5, 40, 27, 450, DateTimeKind.Unspecified).AddTicks(8496), "Developer" },
                    { 4, new DateTime(1353, 9, 20, 4, 14, 34, 578, DateTimeKind.Unspecified).AddTicks(6128), "Paradigm" },
                    { 3, new DateTime(1241, 6, 20, 14, 31, 51, 172, DateTimeKind.Unspecified).AddTicks(3472), "Polarised" },
                    { 2, new DateTime(339, 5, 26, 6, 59, 13, 785, DateTimeKind.Unspecified).AddTicks(7072), "payment" },
                    { 19, new DateTime(1986, 7, 11, 17, 34, 4, 464, DateTimeKind.Unspecified).AddTicks(6480), "Paradigm" },
                    { 20, new DateTime(1075, 9, 4, 13, 43, 1, 10, DateTimeKind.Unspecified).AddTicks(3760), "Auto Loan Account" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1949), "PCI", "Colombian Peso", "Cambridgeshire", new DateTime(1581, 3, 11, 18, 46, 23, 541, DateTimeKind.Unspecified).AddTicks(7168), 1 },
                    { 90, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1986), "Beauty, Clothing & Clothing", "Small Steel Shoes", "Crest", new DateTime(691, 9, 20, 9, 48, 57, 327, DateTimeKind.Unspecified).AddTicks(8480), 14 },
                    { 68, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1985), "Pennsylvania", "Wooden", "Maryland", new DateTime(59, 8, 8, 22, 35, 24, 743, DateTimeKind.Unspecified).AddTicks(420), 14 },
                    { 47, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1981), "Balboa", "cross-platform", "synthesizing", new DateTime(599, 5, 22, 23, 34, 9, 277, DateTimeKind.Unspecified).AddTicks(3776), 14 },
                    { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1981), "Sleek", "invoice", "lavender", new DateTime(527, 10, 1, 7, 30, 40, 319, DateTimeKind.Unspecified).AddTicks(3360), 14 },
                    { 55, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(2001), "US Dollar", "purple", "Handmade Cotton Bike", new DateTime(1013, 4, 22, 4, 45, 0, 440, DateTimeKind.Unspecified).AddTicks(2704), 13 },
                    { 50, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1966), "Security", "Fork", "Soft", new DateTime(1530, 11, 15, 3, 30, 18, 133, DateTimeKind.Unspecified).AddTicks(1536), 13 },
                    { 86, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1975), "world-class", "Administrator", "Tasty Steel Mouse", new DateTime(1231, 4, 18, 0, 31, 44, 533, DateTimeKind.Unspecified).AddTicks(5632), 12 },
                    { 70, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1972), "alliance", "Ergonomic", "Engineer", new DateTime(276, 11, 2, 15, 59, 53, 391, DateTimeKind.Unspecified).AddTicks(4736), 12 },
                    { 49, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(2001), "Creative", "New Mexico", "mesh", new DateTime(919, 7, 21, 23, 33, 5, 387, DateTimeKind.Unspecified).AddTicks(4000), 12 },
                    { 42, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1967), "cross-platform", "Sierra Leone", "cyan", new DateTime(1214, 3, 12, 7, 42, 49, 569, DateTimeKind.Unspecified).AddTicks(4928), 12 },
                    { 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1992), "Legacy", "Analyst", "Lead", new DateTime(101, 3, 11, 14, 18, 37, 443, DateTimeKind.Unspecified).AddTicks(4192), 12 },
                    { 23, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1975), "initiatives", "District", "COM", new DateTime(1968, 10, 19, 17, 52, 58, 934, DateTimeKind.Unspecified).AddTicks(624), 12 },
                    { 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1973), "Rapid", "Tasty Soft Pants", "index", new DateTime(1464, 6, 28, 18, 14, 20, 777, DateTimeKind.Unspecified).AddTicks(3328), 12 },
                    { 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1948), "Investor", "zero administration", "Implemented", new DateTime(1216, 10, 30, 22, 9, 56, 547, DateTimeKind.Unspecified).AddTicks(8480), 12 },
                    { 99, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1960), "haptic", "XSS", "Jewelery, Garden & Grocery", new DateTime(385, 11, 24, 13, 22, 59, 237, DateTimeKind.Unspecified).AddTicks(7504), 11 },
                    { 76, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1969), "architectures", "copy", "Beauty, Outdoors & Beauty", new DateTime(185, 1, 23, 21, 38, 34, 734, DateTimeKind.Unspecified).AddTicks(3264), 11 },
                    { 75, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1959), "indexing", "transmitting", "Generic Soft Shirt", new DateTime(1520, 7, 23, 7, 49, 3, 947, DateTimeKind.Unspecified).AddTicks(3040), 11 },
                    { 73, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(2004), "Intelligent", "well-modulated", "global", new DateTime(590, 9, 15, 22, 11, 42, 429, DateTimeKind.Unspecified).AddTicks(9152), 11 },
                    { 69, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1960), "indigo", "Cambridgeshire", "Cambridgeshire", new DateTime(809, 9, 28, 7, 14, 12, 706, DateTimeKind.Unspecified).AddTicks(752), 11 },
                    { 44, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1957), "Investment Account", "Fantastic Frozen Shirt", "parsing", new DateTime(380, 1, 7, 23, 23, 43, 48, DateTimeKind.Unspecified).AddTicks(1600), 11 },
                    { 40, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1972), "Sleek", "Triple-buffered", "Awesome", new DateTime(1850, 5, 16, 21, 47, 0, 82, DateTimeKind.Unspecified).AddTicks(8880), 11 },
                    { 17, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1950), "Isle of Man", "connecting", "IB", new DateTime(1271, 1, 26, 23, 42, 49, 698, DateTimeKind.Unspecified).AddTicks(1264), 15 },
                    { 31, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1940), "bypassing", "interactive", "Home Loan Account", new DateTime(1980, 7, 21, 8, 17, 56, 451, DateTimeKind.Unspecified).AddTicks(6560), 11 },
                    { 29, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1988), "auxiliary", "Granite", "Nepalese Rupee", new DateTime(1112, 2, 22, 5, 21, 7, 96, DateTimeKind.Unspecified).AddTicks(4944), 15 },
                    { 85, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1948), "bypassing", "Manager", "ivory", new DateTime(391, 7, 7, 21, 55, 3, 320, DateTimeKind.Unspecified).AddTicks(4176), 15 },
                    { 72, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1970), "Ranch", "Infrastructure", "object-oriented", new DateTime(436, 10, 15, 3, 28, 24, 758, DateTimeKind.Unspecified).AddTicks(2352), 20 },
                    { 60, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1987), "EXE", "Palladium", "Kids & Electronics", new DateTime(815, 11, 30, 13, 49, 1, 728, DateTimeKind.Unspecified).AddTicks(3056), 20 },
                    { 25, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(2001), "open architecture", "Landing", "Outdoors, Industrial & Sports", new DateTime(550, 9, 18, 18, 55, 29, 828, DateTimeKind.Unspecified).AddTicks(9936), 20 },
                    { 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(2002), "Stravenue", "Buckinghamshire", "1080p", new DateTime(1865, 8, 9, 13, 39, 15, 535, DateTimeKind.Unspecified).AddTicks(3680), 20 },
                    { 15, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1996), "lime", "cultivate", "Regional", new DateTime(1624, 8, 28, 7, 46, 38, 45, DateTimeKind.Unspecified).AddTicks(8448), 20 },
                    { 11, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(2000), "CSS", "bricks-and-clicks", "revolutionize", new DateTime(1621, 8, 3, 12, 48, 38, 156, DateTimeKind.Unspecified).AddTicks(144), 20 },
                    { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1964), "Parks", "quantifying", "North Korean Won", new DateTime(785, 9, 15, 18, 11, 50, 319, DateTimeKind.Unspecified).AddTicks(4960), 20 },
                    { 67, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1976), "Sleek Granite Pizza", "Buckinghamshire", "Planner", new DateTime(448, 4, 12, 3, 27, 32, 273, DateTimeKind.Unspecified).AddTicks(2576), 19 },
                    { 66, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1987), "Berkshire", "Granite", "Creative", new DateTime(765, 12, 3, 1, 10, 14, 448, DateTimeKind.Unspecified).AddTicks(5872), 19 },
                    { 53, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1996), "enterprise", "24/7", "Ways", new DateTime(6, 7, 3, 23, 12, 51, 386, DateTimeKind.Unspecified).AddTicks(336), 19 },
                    { 13, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1993), "compelling", "Fantastic Soft Tuna", "Coordinator", new DateTime(864, 7, 11, 10, 21, 33, 317, DateTimeKind.Unspecified).AddTicks(3520), 19 },
                    { 94, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1983), "Course", "Buckinghamshire", "Human", new DateTime(1074, 11, 26, 20, 20, 24, 658, DateTimeKind.Unspecified).AddTicks(6064), 18 },
                    { 51, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1996), "Directives", "withdrawal", "Louisiana", new DateTime(832, 7, 10, 23, 55, 15, 245, DateTimeKind.Unspecified).AddTicks(4640), 18 },
                    { 28, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1999), "Delaware", "Tasty Soft Table", "Facilitator", new DateTime(1018, 8, 5, 23, 28, 58, 832, DateTimeKind.Unspecified).AddTicks(912), 18 },
                    { 89, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1969), "Connecticut", "Circles", "Producer", new DateTime(36, 7, 25, 4, 34, 53, 610, DateTimeKind.Unspecified).AddTicks(9016), 17 },
                    { 88, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1977), "JSON", "Architect", "Checking Account", new DateTime(679, 9, 25, 15, 33, 20, 692, DateTimeKind.Unspecified).AddTicks(4208), 17 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 93, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1980), "Industrial & Music", "redundant", "connecting", new DateTime(789, 4, 28, 12, 48, 52, 526, DateTimeKind.Unspecified).AddTicks(496), 16 },
                    { 78, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(2002), "maximize", "Decentralized", "expedite", new DateTime(342, 11, 30, 9, 55, 58, 425, DateTimeKind.Unspecified).AddTicks(9040), 16 },
                    { 71, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1993), "optical", "digital", "payment", new DateTime(407, 3, 2, 22, 27, 1, 640, DateTimeKind.Unspecified).AddTicks(2800), 16 },
                    { 26, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1997), "magenta", "Chief", "tan", new DateTime(1308, 2, 21, 18, 48, 23, 209, DateTimeKind.Unspecified).AddTicks(9536), 16 },
                    { 10, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1974), "RSS", "Architect", "Avon", new DateTime(1144, 3, 3, 8, 56, 12, 105, DateTimeKind.Unspecified).AddTicks(2944), 16 },
                    { 84, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1999), "payment", "Officer", "Outdoors & Beauty", new DateTime(17, 12, 20, 9, 42, 28, 675, DateTimeKind.Unspecified).AddTicks(9815), 15 },
                    { 30, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1963), "Lead", "Convertible Marks", "experiences", new DateTime(731, 8, 17, 7, 25, 55, 800, DateTimeKind.Unspecified).AddTicks(4336), 11 },
                    { 21, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(2004), "Stand-alone", "Mews", "back up", new DateTime(1109, 3, 23, 7, 38, 25, 697, DateTimeKind.Unspecified).AddTicks(4672), 11 },
                    { 18, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1971), "generating", "Specialist", "Music & Books", new DateTime(463, 5, 6, 1, 6, 45, 556, DateTimeKind.Unspecified).AddTicks(656), 11 },
                    { 14, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1981), "Bedfordshire", "Infrastructure", "black", new DateTime(1915, 8, 1, 16, 57, 45, 201, DateTimeKind.Unspecified).AddTicks(5184), 6 },
                    { 59, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1990), "HTTP", "Operations", "adapter", new DateTime(1949, 2, 6, 2, 18, 45, 586, DateTimeKind.Unspecified).AddTicks(2096), 5 },
                    { 43, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1991), "Licensed", "connecting", "CSS", new DateTime(312, 12, 5, 19, 1, 15, 839, DateTimeKind.Unspecified).AddTicks(8688), 5 },
                    { 12, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1993), "Arizona", "SDD", "Intelligent Rubber Shoes", new DateTime(1340, 12, 21, 15, 49, 58, 578, DateTimeKind.Unspecified).AddTicks(9328), 5 },
                    { 100, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1971), "invoice", "Creative", "concept", new DateTime(1632, 3, 13, 8, 3, 5, 986, DateTimeKind.Unspecified).AddTicks(6448), 4 },
                    { 91, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1969), "proactive", "Wooden", "Pike", new DateTime(592, 7, 19, 11, 55, 28, 674, DateTimeKind.Unspecified).AddTicks(2704), 4 },
                    { 82, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1965), "Granite", "deposit", "Gorgeous Plastic Table", new DateTime(1967, 4, 9, 5, 13, 34, 427, DateTimeKind.Unspecified).AddTicks(4256), 4 },
                    { 20, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1946), "Corners", "Markets", "sexy", new DateTime(256, 1, 18, 1, 19, 22, 940, DateTimeKind.Unspecified).AddTicks(5264), 4 },
                    { 58, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1964), "bi-directional", "Malaysia", "Bedfordshire", new DateTime(200, 6, 8, 12, 31, 31, 136, DateTimeKind.Unspecified).AddTicks(8360), 3 },
                    { 48, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1943), "Berkshire", "Engineer", "system-worthy", new DateTime(1860, 4, 10, 20, 39, 33, 295, DateTimeKind.Unspecified).AddTicks(6880), 3 },
                    { 38, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1945), "Alley", "Lead", "USB", new DateTime(666, 3, 26, 21, 16, 7, 113, DateTimeKind.Unspecified).AddTicks(9024), 3 },
                    { 33, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1950), "Applications", "optimize", "relationships", new DateTime(387, 12, 10, 14, 7, 7, 28, DateTimeKind.Unspecified).AddTicks(5216), 3 },
                    { 22, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1957), "Loaf", "Lilangeni", "Lead", new DateTime(1156, 3, 22, 5, 19, 53, 635, DateTimeKind.Unspecified).AddTicks(5536), 3 },
                    { 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1961), "Agent", "Pakistan Rupee", "Netherlands", new DateTime(1606, 12, 6, 11, 13, 13, 723, DateTimeKind.Unspecified).AddTicks(8480), 3 },
                    { 96, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1959), "Regional", "networks", "Alaska", new DateTime(1395, 1, 31, 22, 15, 3, 320, DateTimeKind.Unspecified).AddTicks(1104), 2 },
                    { 81, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1975), "Corner", "hub", "back up", new DateTime(1535, 5, 18, 17, 35, 22, 462, DateTimeKind.Unspecified).AddTicks(4080), 2 },
                    { 52, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1969), "override", "Handcrafted", "Communications", new DateTime(1015, 2, 17, 10, 10, 51, 160, DateTimeKind.Unspecified).AddTicks(6544), 2 },
                    { 34, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1952), "Bedfordshire", "Utah", "Venezuela", new DateTime(1239, 10, 14, 5, 39, 35, 519, DateTimeKind.Unspecified).AddTicks(1376), 2 },
                    { 27, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1985), "Meadows", "Centers", "deposit", new DateTime(395, 9, 22, 22, 46, 7, 609, DateTimeKind.Unspecified).AddTicks(944), 2 },
                    { 9, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1959), "Rustic Fresh Keyboard", "Generic Concrete Towels", "generate", new DateTime(585, 5, 16, 1, 48, 44, 347, DateTimeKind.Unspecified).AddTicks(9120), 2 },
                    { 80, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1962), "Key", "deliver", "Ohio", new DateTime(900, 4, 14, 3, 32, 6, 453, DateTimeKind.Unspecified).AddTicks(3424), 1 },
                    { 37, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1967), "infomediaries", "Soft", "Florida", new DateTime(17, 10, 10, 19, 46, 8, 193, DateTimeKind.Unspecified).AddTicks(7106), 6 },
                    { 56, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1992), "Ranch", "Congo", "platforms", new DateTime(101, 8, 20, 20, 52, 48, 890, DateTimeKind.Unspecified).AddTicks(8928), 6 },
                    { 62, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1999), "3rd generation", "Response", "Ergonomic Cotton Computer", new DateTime(149, 9, 16, 0, 58, 15, 0, DateTimeKind.Unspecified).AddTicks(7920), 6 },
                    { 64, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1997), "Functionality", "mindshare", "array", new DateTime(1042, 2, 13, 12, 53, 4, 213, DateTimeKind.Unspecified).AddTicks(2752), 6 },
                    { 65, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1998), "Andorra", "visionary", "deposit", new DateTime(819, 3, 7, 18, 4, 1, 296, DateTimeKind.Unspecified).AddTicks(7152), 10 },
                    { 45, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1961), "haptic", "white", "Functionality", new DateTime(1688, 10, 24, 23, 48, 8, 906, DateTimeKind.Unspecified).AddTicks(4912), 10 },
                    { 32, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(2000), "ivory", "Director", "Brand", new DateTime(323, 2, 6, 7, 56, 12, 698, DateTimeKind.Unspecified).AddTicks(4096), 10 },
                    { 87, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1969), "Rustic Soft Shoes", "Walk", "Radial", new DateTime(1913, 5, 30, 18, 3, 10, 25, DateTimeKind.Unspecified).AddTicks(832), 9 },
                    { 79, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(2005), "Honduras", "Investment Account", "Architect", new DateTime(1699, 1, 18, 17, 57, 22, 779, DateTimeKind.Unspecified).AddTicks(4640), 9 },
                    { 61, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1990), "Zimbabwe Dollar", "Architect", "Auto Loan Account", new DateTime(821, 5, 3, 11, 14, 2, 429, DateTimeKind.Unspecified).AddTicks(480), 9 },
                    { 35, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1977), "Auto Loan Account", "Awesome", "payment", new DateTime(1207, 9, 4, 5, 9, 17, 256, DateTimeKind.Unspecified).AddTicks(2128), 9 },
                    { 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1950), "National", "Centralized", "Human", new DateTime(1316, 7, 28, 13, 0, 14, 287, DateTimeKind.Unspecified).AddTicks(8928), 9 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 95, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1944), "sensor", "models", "Yemeni Rial", new DateTime(1285, 8, 13, 15, 27, 20, 689, DateTimeKind.Unspecified).AddTicks(7296), 8 },
                    { 77, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1953), "Practical", "haptic", "United Kingdom", new DateTime(587, 8, 13, 9, 0, 59, 167, DateTimeKind.Unspecified).AddTicks(2752), 8 },
                    { 97, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1947), "Gorgeous Concrete Soap", "Summit", "Intelligent Steel Chicken", new DateTime(182, 11, 7, 23, 1, 38, 362, DateTimeKind.Unspecified).AddTicks(7600), 20 },
                    { 63, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(2000), "RAM", "Lek", "Agent", new DateTime(1876, 12, 5, 21, 34, 56, 802, DateTimeKind.Unspecified).AddTicks(3376), 8 },
                    { 41, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1984), "Falls", "JSON", "Frozen", new DateTime(1332, 2, 20, 14, 27, 58, 380, DateTimeKind.Unspecified).AddTicks(6032), 8 },
                    { 39, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1981), "Identity", "Buckinghamshire", "Plains", new DateTime(152, 2, 20, 17, 14, 28, 618, DateTimeKind.Unspecified).AddTicks(368), 8 },
                    { 36, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1940), "parse", "North Carolina", "SMTP", new DateTime(1783, 2, 10, 2, 28, 36, 204, DateTimeKind.Unspecified).AddTicks(9936), 8 },
                    { 19, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1970), "Usability", "Fork", "Locks", new DateTime(2016, 7, 20, 2, 29, 35, 858, DateTimeKind.Unspecified).AddTicks(176), 8 },
                    { 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1941), "lavender", "olive", "Refined", new DateTime(211, 6, 29, 15, 20, 58, 157, DateTimeKind.Unspecified).AddTicks(7336), 8 },
                    { 92, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1961), "Unbranded", "Handmade Frozen Hat", "Developer", new DateTime(272, 12, 9, 23, 9, 52, 253, DateTimeKind.Unspecified).AddTicks(4160), 7 },
                    { 83, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1977), "Dynamic", "Music", "Practical", new DateTime(379, 6, 8, 11, 53, 31, 928, DateTimeKind.Unspecified).AddTicks(9712), 7 },
                    { 74, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1954), "analyzing", "Vatu", "connecting", new DateTime(1974, 7, 6, 11, 51, 48, 244, DateTimeKind.Unspecified).AddTicks(2192), 7 },
                    { 57, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1994), "solid state", "Licensed", "bandwidth", new DateTime(1191, 12, 9, 4, 32, 36, 166, DateTimeKind.Unspecified).AddTicks(9392), 7 },
                    { 46, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1974), "Cloned", "Land", "Branding", new DateTime(2005, 4, 14, 22, 34, 1, 986, DateTimeKind.Unspecified).AddTicks(1712), 7 },
                    { 54, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1953), "multi-state", "Home", "Ergonomic Wooden Shoes", new DateTime(54, 1, 30, 2, 18, 53, 288, DateTimeKind.Unspecified).AddTicks(7834), 8 },
                    { 98, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified).AddTicks(1990), "Handmade", "grow", "Markets", new DateTime(1777, 2, 3, 10, 5, 12, 725, DateTimeKind.Unspecified).AddTicks(9728), 20 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 6, 3, new DateTime(1356, 11, 28, 10, 51, 36, 280, DateTimeKind.Unspecified).AddTicks(6736), new DateTime(1813, 11, 25, 8, 59, 26, 251, DateTimeKind.Unspecified).AddTicks(4896), "Canadian Dollar Ergonomic Granite Hat Lights", "customized", 17 },
                    { 13, 15, new DateTime(1784, 12, 20, 20, 1, 55, 804, DateTimeKind.Unspecified).AddTicks(976), new DateTime(395, 10, 6, 8, 53, 19, 596, DateTimeKind.Unspecified).AddTicks(9600), "Berkshire Road Squares", "Shoes & Clothing", 19 },
                    { 12, 11, new DateTime(644, 1, 18, 1, 1, 58, 248, DateTimeKind.Unspecified).AddTicks(9360), new DateTime(1600, 8, 19, 9, 17, 38, 870, DateTimeKind.Unspecified).AddTicks(6704), "Money Market Account CSS International", "Moroccan Dirham", 9 },
                    { 1, 11, new DateTime(1944, 11, 7, 14, 35, 56, 394, DateTimeKind.Unspecified).AddTicks(4912), new DateTime(833, 1, 25, 17, 3, 50, 204, DateTimeKind.Unspecified).AddTicks(3824), "Strategist content Ergonomic", "Assimilated", 6 },
                    { 2, 2, new DateTime(1610, 2, 15, 16, 28, 24, 566, DateTimeKind.Unspecified).AddTicks(9904), new DateTime(1711, 1, 1, 22, 35, 49, 755, DateTimeKind.Unspecified).AddTicks(928), "Liaison Ergonomic Frozen Pizza redundant", "red", 18 },
                    { 14, 13, new DateTime(577, 3, 10, 7, 59, 7, 263, DateTimeKind.Unspecified).AddTicks(6496), new DateTime(1200, 3, 11, 11, 13, 36, 513, DateTimeKind.Unspecified).AddTicks(8640), "Ergonomic Granite Hat National Springs", "Borders", 6 },
                    { 7, 13, new DateTime(1741, 4, 9, 19, 1, 0, 49, DateTimeKind.Unspecified).AddTicks(4672), new DateTime(1814, 10, 1, 10, 18, 44, 389, DateTimeKind.Unspecified).AddTicks(896), "online Fresh deposit", "XML", 11 },
                    { 15, 10, new DateTime(1464, 6, 5, 4, 52, 30, 130, DateTimeKind.Unspecified).AddTicks(3376), new DateTime(279, 5, 13, 15, 18, 49, 791, DateTimeKind.Unspecified).AddTicks(6576), "overriding Home monitor", "Home Loan Account", 8 },
                    { 5, 10, new DateTime(1657, 9, 8, 19, 21, 1, 139, DateTimeKind.Unspecified).AddTicks(1632), new DateTime(461, 1, 24, 21, 32, 51, 640, DateTimeKind.Unspecified).AddTicks(8304), "invoice Associate Awesome Frozen Table", "Awesome Steel Chips", 8 },
                    { 9, 1, new DateTime(1880, 8, 25, 8, 22, 22, 978, DateTimeKind.Unspecified).AddTicks(9776), new DateTime(1168, 12, 27, 12, 27, 29, 913, DateTimeKind.Unspecified).AddTicks(2048), "Programmable Fantastic Plastic Chicken Borders", "Books, Toys & Tools", 5 },
                    { 16, 8, new DateTime(881, 2, 17, 5, 56, 17, 177, DateTimeKind.Unspecified).AddTicks(9664), new DateTime(1919, 4, 21, 23, 5, 26, 124, DateTimeKind.Unspecified).AddTicks(7312), "Forward withdrawal Swedish Krona", "withdrawal", 13 },
                    { 20, 6, new DateTime(1094, 12, 5, 17, 16, 56, 444, DateTimeKind.Unspecified).AddTicks(7504), new DateTime(1573, 9, 10, 22, 39, 4, 628, DateTimeKind.Unspecified).AddTicks(3408), "navigate Rubber Awesome Granite Sausages", "Personal Loan Account", 16 },
                    { 3, 7, new DateTime(1952, 5, 5, 4, 34, 15, 513, DateTimeKind.Unspecified).AddTicks(2240), new DateTime(116, 1, 30, 4, 39, 56, 244, DateTimeKind.Unspecified).AddTicks(4192), "Savings Account Rue Gorgeous", "Ports", 17 },
                    { 8, 12, new DateTime(423, 11, 10, 0, 57, 18, 541, DateTimeKind.Unspecified).AddTicks(6816), new DateTime(1394, 12, 26, 5, 18, 32, 90, DateTimeKind.Unspecified).AddTicks(8816), "engineer Chief Alley", "JBOD", 2 },
                    { 17, 20, new DateTime(1854, 1, 5, 22, 16, 40, 228, DateTimeKind.Unspecified).AddTicks(272), new DateTime(1904, 6, 9, 16, 23, 45, 517, DateTimeKind.Unspecified).AddTicks(5376), "invoice Security Compatible", "solid state", 3 },
                    { 4, 20, new DateTime(1691, 3, 3, 18, 58, 5, 395, DateTimeKind.Unspecified).AddTicks(6560), new DateTime(353, 12, 12, 1, 36, 54, 224, DateTimeKind.Unspecified).AddTicks(48), "Horizontal demand-driven Total", "Maine", 19 },
                    { 19, 9, new DateTime(443, 9, 29, 1, 17, 17, 69, DateTimeKind.Unspecified).AddTicks(1648), new DateTime(259, 12, 3, 10, 37, 11, 372, DateTimeKind.Unspecified).AddTicks(6592), "Configuration Trail tangible", "Zimbabwe Dollar", 6 },
                    { 11, 3, new DateTime(764, 9, 19, 18, 28, 36, 217, DateTimeKind.Unspecified).AddTicks(9280), new DateTime(1717, 7, 8, 16, 17, 50, 458, DateTimeKind.Unspecified).AddTicks(4464), "virtual Cotton fuchsia", "withdrawal", 15 },
                    { 18, 15, new DateTime(480, 10, 2, 21, 34, 23, 136, DateTimeKind.Unspecified).AddTicks(4144), new DateTime(1916, 7, 13, 21, 0, 8, 39, DateTimeKind.Unspecified).AddTicks(736), "JSON transmit Oklahoma", "Cambridgeshire", 8 },
                    { 10, 16, new DateTime(442, 3, 31, 20, 28, 57, 289, DateTimeKind.Unspecified).AddTicks(9520), new DateTime(741, 8, 23, 8, 37, 51, 606, DateTimeKind.Unspecified).AddTicks(1680), "black context-sensitive Florida", "Extended", 11 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 14, new DateTime(1239, 10, 6, 8, 5, 49, 59, DateTimeKind.Unspecified).AddTicks(672), "upward-trending experiences Trace", new DateTime(267, 3, 31, 5, 32, 53, 160, DateTimeKind.Unspecified).AddTicks(6000), "transparent", 26, 6, 2 },
                    { 81, new DateTime(1060, 2, 15, 2, 47, 35, 487, DateTimeKind.Unspecified).AddTicks(288), "Devolved monitor leading-edge", new DateTime(1508, 2, 21, 11, 18, 46, 915, DateTimeKind.Unspecified).AddTicks(9568), "olive", 94, 14, 1 },
                    { 19, new DateTime(700, 11, 28, 11, 1, 9, 512, DateTimeKind.Unspecified).AddTicks(6640), "Applications pink index", new DateTime(1864, 12, 11, 23, 19, 40, 550, DateTimeKind.Unspecified).AddTicks(9456), "access", 50, 14, 4 },
                    { 13, new DateTime(279, 6, 30, 4, 20, 15, 601, DateTimeKind.Unspecified).AddTicks(832), "generating olive port", new DateTime(1334, 7, 2, 23, 26, 15, 457, DateTimeKind.Unspecified).AddTicks(9728), "markets", 46, 14, 1 },
                    { 10, new DateTime(1948, 6, 29, 8, 59, 48, 886, DateTimeKind.Unspecified).AddTicks(4336), "Guyana extend Malaysian Ringgit", new DateTime(353, 7, 1, 21, 40, 42, 46, DateTimeKind.Unspecified).AddTicks(7072), "protocol", 73, 14, 4 },
                    { 296, new DateTime(1186, 10, 23, 10, 42, 51, 988, DateTimeKind.Unspecified).AddTicks(4496), "sensor protocol Hawaii", new DateTime(46, 2, 7, 4, 19, 40, 262, DateTimeKind.Unspecified).AddTicks(2172), "Cook Islands", 4, 7, 4 },
                    { 295, new DateTime(209, 2, 19, 21, 29, 14, 135, DateTimeKind.Unspecified).AddTicks(8360), "redundant Buckinghamshire Danish Krone", new DateTime(1089, 12, 9, 23, 3, 26, 300, DateTimeKind.Unspecified).AddTicks(5904), "envisioneer", 93, 7, 2 },
                    { 110, new DateTime(341, 10, 2, 8, 43, 10, 776, DateTimeKind.Unspecified).AddTicks(2448), "Viaduct maximized Movies & Electronics", new DateTime(11, 12, 22, 5, 16, 41, 882, DateTimeKind.Unspecified).AddTicks(7348), "Dynamic", 91, 14, 2 },
                    { 293, new DateTime(1507, 9, 24, 6, 27, 58, 997, DateTimeKind.Unspecified).AddTicks(320), "USB system Afghanistan", new DateTime(1147, 11, 10, 15, 6, 54, 813, DateTimeKind.Unspecified).AddTicks(3712), "Buckinghamshire", 44, 7, 3 },
                    { 272, new DateTime(1121, 8, 19, 19, 36, 39, 896, DateTimeKind.Unspecified).AddTicks(848), "orange interface Managed", new DateTime(764, 12, 15, 21, 54, 49, 582, DateTimeKind.Unspecified).AddTicks(9680), "Arizona", 10, 7, 1 },
                    { 251, new DateTime(417, 5, 22, 14, 31, 52, 331, DateTimeKind.Unspecified).AddTicks(9152), "Savings Account context-sensitive Burundi Franc", new DateTime(727, 6, 9, 13, 33, 54, 381, DateTimeKind.Unspecified), "Alaska", 97, 7, 2 },
                    { 246, new DateTime(170, 7, 4, 0, 34, 44, 354, DateTimeKind.Unspecified).AddTicks(6944), "open architecture Compatible Drive", new DateTime(1736, 10, 25, 2, 26, 50, 905, DateTimeKind.Unspecified).AddTicks(192), "Adaptive", 66, 7, 1 },
                    { 240, new DateTime(970, 8, 20, 23, 55, 4, 984, DateTimeKind.Unspecified).AddTicks(3920), "out-of-the-box Handmade Concrete Shirt AI", new DateTime(993, 6, 2, 5, 22, 27, 16, DateTimeKind.Unspecified).AddTicks(4944), "Netherlands Antilles", 77, 7, 1 },
                    { 234, new DateTime(1397, 3, 8, 14, 20, 5, 305, DateTimeKind.Unspecified).AddTicks(9664), "Alabama Gibraltar Pound generate", new DateTime(1794, 10, 13, 0, 36, 1, 686, DateTimeKind.Unspecified).AddTicks(5168), "Identity", 52, 7, 1 },
                    { 187, new DateTime(239, 12, 24, 14, 9, 13, 166, DateTimeKind.Unspecified).AddTicks(2560), "architectures payment matrix", new DateTime(355, 9, 7, 15, 18, 3, 539, DateTimeKind.Unspecified).AddTicks(8032), "quantify", 64, 7, 2 },
                    { 274, new DateTime(15, 8, 24, 2, 43, 44, 706, DateTimeKind.Unspecified).AddTicks(11), "monitoring platforms back-end", new DateTime(1916, 5, 25, 15, 53, 4, 493, DateTimeKind.Unspecified).AddTicks(8832), "Auto Loan Account", 38, 7, 2 },
                    { 120, new DateTime(1130, 9, 10, 4, 48, 15, 185, DateTimeKind.Unspecified).AddTicks(5952), "directional Canada ROI", new DateTime(1077, 12, 25, 1, 22, 1, 737, DateTimeKind.Unspecified).AddTicks(9024), "Cambridgeshire", 3, 14, 1 },
                    { 204, new DateTime(1432, 1, 7, 1, 24, 13, 287, DateTimeKind.Unspecified).AddTicks(3872), "benchmark Unbranded open-source", new DateTime(1388, 5, 7, 16, 22, 30, 845, DateTimeKind.Unspecified).AddTicks(4160), "Handmade Frozen Shoes", 95, 14, 1 },
                    { 218, new DateTime(585, 7, 25, 0, 25, 53, 560, DateTimeKind.Unspecified).AddTicks(5168), "Handmade Steel Gloves web-readiness impactful", new DateTime(1262, 12, 11, 4, 26, 49, 89, DateTimeKind.Unspecified).AddTicks(9088), "Argentine Peso", 24, 14, 2 },
                    { 132, new DateTime(720, 9, 2, 2, 37, 54, 676, DateTimeKind.Unspecified).AddTicks(2448), "Central African Republic Applications Circle", new DateTime(1339, 8, 18, 4, 50, 3, 63, DateTimeKind.Unspecified).AddTicks(8544), "frictionless", 2, 2, 3 },
                    { 122, new DateTime(1235, 12, 10, 5, 38, 40, 268, DateTimeKind.Unspecified).AddTicks(656), "Jamaican Dollar enterprise iterate", new DateTime(1958, 8, 8, 8, 41, 44, 429, DateTimeKind.Unspecified).AddTicks(6784), "Awesome Cotton Mouse", 95, 2, 3 },
                    { 112, new DateTime(366, 6, 6, 18, 48, 34, 736, DateTimeKind.Unspecified).AddTicks(7008), "best-of-breed world-class British Indian Ocean Territory (Chagos Archipelago)", new DateTime(148, 9, 27, 4, 35, 19, 999, DateTimeKind.Unspecified).AddTicks(8136), "Berkshire", 100, 2, 1 },
                    { 93, new DateTime(454, 7, 4, 22, 30, 0, 792, DateTimeKind.Unspecified).AddTicks(1776), "Clothing system Fundamental", new DateTime(1868, 12, 14, 16, 26, 59, 94, DateTimeKind.Unspecified).AddTicks(6512), "back up", 99, 2, 1 },
                    { 83, new DateTime(1897, 5, 29, 22, 17, 1, 344, DateTimeKind.Unspecified).AddTicks(5712), "Divide withdrawal back-end", new DateTime(1478, 10, 28, 20, 25, 24, 497, DateTimeKind.Unspecified).AddTicks(1536), "attitude", 8, 2, 4 },
                    { 67, new DateTime(214, 5, 15, 8, 20, 21, 295, DateTimeKind.Unspecified).AddTicks(8320), "grow synergies contextually-based", new DateTime(1618, 9, 19, 18, 47, 12, 138, DateTimeKind.Unspecified).AddTicks(8176), "Books & Tools", 14, 2, 3 },
                    { 40, new DateTime(340, 3, 15, 2, 4, 43, 973, DateTimeKind.Unspecified).AddTicks(9792), "Florida Gorgeous Steel Soap Pennsylvania", new DateTime(212, 2, 12, 22, 19, 5, 123, DateTimeKind.Unspecified).AddTicks(888), "Common", 39, 2, 3 },
                    { 9, new DateTime(1546, 2, 24, 21, 53, 31, 691, DateTimeKind.Unspecified).AddTicks(2080), "24/365 compressing application", new DateTime(1382, 1, 1, 3, 40, 4, 615, DateTimeKind.Unspecified).AddTicks(8352), "haptic", 66, 2, 2 },
                    { 6, new DateTime(1209, 3, 22, 8, 38, 44, 247, DateTimeKind.Unspecified).AddTicks(8608), "Gorgeous Frozen Bacon Manager Solutions", new DateTime(290, 7, 1, 15, 19, 55, 387, DateTimeKind.Unspecified).AddTicks(2496), "even-keeled", 91, 2, 2 },
                    { 5, new DateTime(464, 1, 23, 7, 57, 56, 465, DateTimeKind.Unspecified).AddTicks(3776), "incentivize Brand deposit", new DateTime(1678, 7, 6, 23, 14, 1, 550, DateTimeKind.Unspecified).AddTicks(5808), "Engineer", 19, 2, 1 },
                    { 267, new DateTime(1735, 10, 11, 6, 47, 47, 102, DateTimeKind.Unspecified).AddTicks(4400), "Investment Account magenta Borders", new DateTime(617, 3, 1, 0, 55, 13, 907, DateTimeKind.Unspecified).AddTicks(7040), "dynamic", 47, 14, 4 },
                    { 262, new DateTime(910, 5, 13, 5, 21, 42, 714, DateTimeKind.Unspecified).AddTicks(9232), "Georgia capacitor Handmade Concrete Sausages", new DateTime(258, 4, 14, 9, 19, 19, 374, DateTimeKind.Unspecified).AddTicks(9536), "CSS", 59, 14, 1 },
                    { 239, new DateTime(1058, 4, 28, 22, 4, 1, 350, DateTimeKind.Unspecified).AddTicks(2672), "Functionality withdrawal Intelligent Fresh Pizza", new DateTime(870, 10, 9, 8, 43, 28, 791, DateTimeKind.Unspecified).AddTicks(1248), "Forks", 83, 14, 3 },
                    { 225, new DateTime(985, 10, 28, 10, 0, 16, 237, DateTimeKind.Unspecified).AddTicks(1536), "Virginia e-markets Strategist", new DateTime(224, 1, 19, 3, 28, 20, 483, DateTimeKind.Unspecified).AddTicks(6448), "RAM", 91, 14, 4 },
                    { 220, new DateTime(1847, 2, 21, 19, 49, 57, 741, DateTimeKind.Unspecified).AddTicks(7552), "array Sleek Ergonomic", new DateTime(181, 9, 8, 10, 0, 36, 515, DateTimeKind.Unspecified).AddTicks(1848), "solid state", 80, 14, 1 },
                    { 161, new DateTime(302, 12, 19, 3, 52, 32, 636, DateTimeKind.Unspecified).AddTicks(9360), "Croatia FTP SMS", new DateTime(447, 12, 3, 2, 37, 7, 497, DateTimeKind.Unspecified).AddTicks(3008), "Incredible Frozen Ball", 83, 7, 1 },
                    { 155, new DateTime(471, 9, 30, 10, 29, 51, 63, DateTimeKind.Unspecified).AddTicks(7296), "Dale Cove purple", new DateTime(1820, 12, 24, 0, 52, 21, 37, DateTimeKind.Unspecified).AddTicks(960), "Shore", 82, 7, 3 },
                    { 146, new DateTime(1598, 2, 11, 13, 15, 32, 228, DateTimeKind.Unspecified).AddTicks(4368), "South Dakota Cotton transparent", new DateTime(65, 4, 14, 14, 25, 25, 372, DateTimeKind.Unspecified).AddTicks(3136), "Venezuela", 64, 7, 1 },
                    { 131, new DateTime(52, 5, 7, 18, 37, 54, 438, DateTimeKind.Unspecified).AddTicks(3584), "Fundamental Wooden Steel", new DateTime(1300, 3, 10, 0, 44, 59, 130, DateTimeKind.Unspecified).AddTicks(4208), "connect", 84, 7, 1 },
                    { 29, new DateTime(1495, 6, 8, 16, 18, 16, 724, DateTimeKind.Unspecified).AddTicks(8080), "withdrawal invoice Savings Account", new DateTime(148, 3, 2, 14, 14, 26, 137, DateTimeKind.Unspecified).AddTicks(2352), "Frozen", 91, 15, 2 },
                    { 11, new DateTime(216, 11, 27, 15, 11, 46, 148, DateTimeKind.Unspecified).AddTicks(7720), "Investment Account Groves District", new DateTime(888, 4, 7, 2, 50, 52, 544, DateTimeKind.Unspecified).AddTicks(8528), "Automotive", 10, 15, 1 },
                    { 297, new DateTime(408, 4, 3, 9, 8, 56, 512, DateTimeKind.Unspecified).AddTicks(1600), "Principal Plastic schemas", new DateTime(407, 3, 3, 13, 37, 18, 331, DateTimeKind.Unspecified).AddTicks(1024), "engage", 98, 5, 1 },
                    { 280, new DateTime(466, 4, 28, 3, 53, 55, 238, DateTimeKind.Unspecified).AddTicks(3472), "programming Unbranded Vista", new DateTime(2013, 9, 19, 13, 11, 33, 354, DateTimeKind.Unspecified).AddTicks(6832), "methodical", 88, 5, 3 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 215, new DateTime(332, 12, 21, 9, 2, 23, 16, DateTimeKind.Unspecified).AddTicks(5552), "South Dakota Handcrafted Soft Keyboard FTP", new DateTime(70, 8, 30, 16, 32, 34, 708, DateTimeKind.Unspecified).AddTicks(3920), "Massachusetts", 100, 5, 2 },
                    { 156, new DateTime(1111, 11, 6, 12, 6, 33, 287, DateTimeKind.Unspecified).AddTicks(6816), "Generic Shoals program", new DateTime(897, 7, 5, 21, 30, 41, 890, DateTimeKind.Unspecified).AddTicks(1776), "Bedfordshire", 88, 5, 4 },
                    { 117, new DateTime(278, 12, 20, 6, 16, 32, 923, DateTimeKind.Unspecified).AddTicks(1312), "parallelism Practical Fresh Shoes Organized", new DateTime(356, 4, 7, 1, 44, 58, 420, DateTimeKind.Unspecified).AddTicks(768), "open-source", 13, 5, 4 },
                    { 96, new DateTime(525, 5, 8, 1, 1, 1, 826, DateTimeKind.Unspecified).AddTicks(5776), "Sleek AI Consultant", new DateTime(1257, 2, 23, 23, 11, 59, 749, DateTimeKind.Unspecified).AddTicks(3712), "Analyst", 60, 5, 4 },
                    { 75, new DateTime(1802, 3, 22, 13, 27, 12, 356, DateTimeKind.Unspecified).AddTicks(1744), "tangible Investment Account Solomon Islands", new DateTime(500, 4, 28, 21, 32, 20, 99, DateTimeKind.Unspecified).AddTicks(9696), "Pula", 36, 5, 1 },
                    { 291, new DateTime(1309, 7, 29, 13, 2, 5, 400, DateTimeKind.Unspecified).AddTicks(656), "Rustic Steel Chips Technician Generic Cotton Hat", new DateTime(1492, 10, 12, 1, 40, 47, 860, DateTimeKind.Unspecified).AddTicks(2000), "mission-critical", 43, 9, 1 },
                    { 277, new DateTime(731, 3, 2, 19, 4, 41, 816, DateTimeKind.Unspecified).AddTicks(8080), "SSL Shoals parsing", new DateTime(704, 9, 16, 13, 54, 14, 951, DateTimeKind.Unspecified).AddTicks(3520), "Customer-focused", 15, 9, 2 },
                    { 276, new DateTime(827, 1, 7, 5, 28, 40, 455, DateTimeKind.Unspecified).AddTicks(5408), "Open-source synthesize Interactions", new DateTime(239, 10, 21, 14, 7, 23, 612, DateTimeKind.Unspecified).AddTicks(1472), "high-level", 83, 9, 4 },
                    { 254, new DateTime(1521, 10, 4, 11, 6, 21, 698, DateTimeKind.Unspecified).AddTicks(8688), "mission-critical complexity payment", new DateTime(1575, 6, 19, 17, 25, 34, 717, DateTimeKind.Unspecified).AddTicks(6528), "orange", 46, 9, 1 },
                    { 253, new DateTime(468, 3, 27, 19, 50, 27, 621, DateTimeKind.Unspecified).AddTicks(8512), "Applications alarm Mauritius", new DateTime(624, 12, 23, 13, 27, 24, 435, DateTimeKind.Unspecified).AddTicks(4160), "cultivate", 63, 9, 1 },
                    { 201, new DateTime(432, 2, 22, 6, 59, 14, 315, DateTimeKind.Unspecified).AddTicks(5504), "directional New Jersey SMS", new DateTime(1878, 7, 20, 3, 45, 57, 735, DateTimeKind.Unspecified).AddTicks(3168), "Kwacha", 81, 9, 4 },
                    { 50, new DateTime(1363, 3, 12, 10, 26, 3, 436, DateTimeKind.Unspecified).AddTicks(8464), "lavender navigate Georgia", new DateTime(380, 11, 3, 19, 20, 5, 201, DateTimeKind.Unspecified).AddTicks(2016), "Paradigm", 98, 15, 2 },
                    { 154, new DateTime(1164, 5, 30, 5, 24, 2, 466, DateTimeKind.Unspecified).AddTicks(3440), "Tasty Concrete Chicken Reactive primary", new DateTime(505, 5, 12, 2, 13, 28, 601, DateTimeKind.Unspecified).AddTicks(4544), "Director", 66, 2, 3 },
                    { 53, new DateTime(1908, 9, 2, 3, 36, 24, 866, DateTimeKind.Unspecified).AddTicks(3504), "ADP programming transmitter", new DateTime(1555, 3, 19, 12, 48, 32, 924, DateTimeKind.Unspecified).AddTicks(2512), "technologies", 91, 15, 2 },
                    { 62, new DateTime(108, 2, 14, 8, 24, 32, 990, DateTimeKind.Unspecified).AddTicks(3012), "lime Rubber mint green", new DateTime(1635, 10, 27, 2, 5, 18, 764, DateTimeKind.Unspecified).AddTicks(1744), "Interactions", 23, 15, 2 },
                    { 125, new DateTime(1637, 3, 8, 8, 57, 37, 681, DateTimeKind.Unspecified).AddTicks(1088), "portals application Texas", new DateTime(1410, 5, 11, 5, 35, 45, 497, DateTimeKind.Unspecified).AddTicks(8896), "silver", 97, 7, 3 },
                    { 115, new DateTime(923, 1, 5, 5, 2, 38, 491, DateTimeKind.Unspecified).AddTicks(2080), "Fantastic Granite Bike Personal Loan Account Enhanced", new DateTime(1834, 4, 2, 18, 13, 28, 790, DateTimeKind.Unspecified).AddTicks(3056), "bypassing", 44, 7, 1 },
                    { 101, new DateTime(1313, 7, 30, 20, 19, 57, 535, DateTimeKind.Unspecified).AddTicks(3296), "Steel Director Colorado", new DateTime(495, 6, 5, 12, 31, 48, 853, DateTimeKind.Unspecified).AddTicks(5024), "ADP", 51, 7, 2 },
                    { 88, new DateTime(129, 5, 20, 20, 28, 56, 371, DateTimeKind.Unspecified).AddTicks(9120), "hack Cambridgeshire Shoes", new DateTime(1658, 6, 4, 16, 39, 52, 819, DateTimeKind.Unspecified).AddTicks(4320), "Minnesota", 38, 7, 1 },
                    { 80, new DateTime(1102, 6, 15, 6, 33, 37, 250, DateTimeKind.Unspecified).AddTicks(1968), "Frozen Cambodia Handcrafted", new DateTime(709, 6, 11, 15, 39, 23, 665, DateTimeKind.Unspecified).AddTicks(3168), "Maine", 78, 7, 1 },
                    { 44, new DateTime(1306, 8, 12, 23, 53, 36, 275, DateTimeKind.Unspecified).AddTicks(6816), "Concrete Markets pink", new DateTime(139, 2, 5, 6, 16, 2, 863, DateTimeKind.Unspecified).AddTicks(2568), "Music & Outdoors", 59, 7, 3 },
                    { 22, new DateTime(850, 10, 12, 14, 12, 54, 659, DateTimeKind.Unspecified).AddTicks(6464), "payment red payment", new DateTime(1507, 12, 20, 16, 30, 11, 657, DateTimeKind.Unspecified).AddTicks(9792), "EXE", 73, 7, 1 },
                    { 18, new DateTime(1599, 3, 21, 1, 5, 19, 331, DateTimeKind.Unspecified).AddTicks(9312), "grid-enabled Garden, Health & Games Flat", new DateTime(1881, 5, 8, 6, 34, 51, 2, DateTimeKind.Unspecified).AddTicks(8368), "withdrawal", 78, 7, 2 },
                    { 282, new DateTime(1276, 8, 7, 7, 4, 55, 983, DateTimeKind.Unspecified).AddTicks(416), "Cambridgeshire encompassing Investor", new DateTime(1496, 8, 8, 16, 51, 42, 399, DateTimeKind.Unspecified).AddTicks(4704), "Paradigm", 25, 15, 4 },
                    { 281, new DateTime(1654, 8, 31, 6, 22, 57, 631, DateTimeKind.Unspecified).AddTicks(736), "Louisiana mobile concept", new DateTime(57, 5, 31, 15, 5, 34, 573, DateTimeKind.Unspecified).AddTicks(1814), "overriding", 91, 15, 2 },
                    { 269, new DateTime(621, 11, 7, 3, 35, 19, 359, DateTimeKind.Unspecified).AddTicks(4992), "Swaziland Small Jewelery & Electronics", new DateTime(1773, 6, 16, 5, 38, 16, 217, DateTimeKind.Unspecified).AddTicks(768), "neutral", 8, 15, 2 },
                    { 152, new DateTime(1299, 4, 17, 19, 18, 36, 15, DateTimeKind.Unspecified).AddTicks(9120), "Open-source mindshare lavender", new DateTime(950, 7, 8, 5, 50, 54, 14, DateTimeKind.Unspecified).AddTicks(3120), "Roads", 73, 15, 3 },
                    { 139, new DateTime(1952, 2, 2, 6, 2, 5, 229, DateTimeKind.Unspecified).AddTicks(640), "olive capacitor Chief", new DateTime(1328, 5, 30, 13, 29, 20, 220, DateTimeKind.Unspecified).AddTicks(7760), "Implementation", 49, 15, 1 },
                    { 107, new DateTime(1561, 5, 26, 3, 55, 43, 994, DateTimeKind.Unspecified).AddTicks(2416), "target synthesize Estates", new DateTime(420, 8, 19, 20, 1, 48, 760, DateTimeKind.Unspecified).AddTicks(3200), "wireless", 75, 15, 1 },
                    { 72, new DateTime(860, 5, 18, 18, 41, 51, 927, DateTimeKind.Unspecified).AddTicks(5504), "Markets Agent Fantastic Rubber Pizza", new DateTime(120, 1, 15, 22, 56, 32, 800, DateTimeKind.Unspecified).AddTicks(1072), "partnerships", 19, 15, 4 },
                    { 55, new DateTime(1858, 4, 25, 21, 29, 49, 73, DateTimeKind.Unspecified).AddTicks(7488), "Refined Fresh Car Rubber Program", new DateTime(1404, 2, 11, 3, 44, 27, 299, DateTimeKind.Unspecified).AddTicks(2592), "synthesize", 73, 15, 1 },
                    { 195, new DateTime(1373, 3, 5, 15, 13, 56, 897, DateTimeKind.Unspecified).AddTicks(1472), "Springs Investment Account eyeballs", new DateTime(191, 5, 30, 0, 59, 17, 707, DateTimeKind.Unspecified).AddTicks(6672), "zero defect", 41, 9, 4 },
                    { 173, new DateTime(1589, 9, 30, 4, 45, 45, 390, DateTimeKind.Unspecified).AddTicks(5808), "Fundamental circuit functionalities", new DateTime(573, 3, 4, 3, 58, 41, 625, DateTimeKind.Unspecified).AddTicks(1888), "Summit", 19, 2, 1 },
                    { 235, new DateTime(1670, 7, 2, 22, 52, 48, 465, DateTimeKind.Unspecified).AddTicks(2880), "4th generation Plains SDD", new DateTime(476, 7, 31, 16, 31, 7, 907, DateTimeKind.Unspecified).AddTicks(8896), "Product", 26, 2, 3 },
                    { 242, new DateTime(1371, 6, 27, 13, 44, 52, 451, DateTimeKind.Unspecified).AddTicks(7008), "bandwidth Orchestrator FTP", new DateTime(1024, 10, 10, 15, 7, 1, 750, DateTimeKind.Unspecified).AddTicks(9200), "Tennessee", 34, 18, 2 },
                    { 237, new DateTime(1457, 6, 12, 3, 26, 42, 699, DateTimeKind.Unspecified).AddTicks(4128), "Avon functionalities synthesizing", new DateTime(799, 9, 23, 10, 33, 56, 476, DateTimeKind.Unspecified).AddTicks(5680), "driver", 39, 18, 3 },
                    { 199, new DateTime(366, 6, 3, 20, 33, 16, 998, DateTimeKind.Unspecified).AddTicks(7376), "Bedfordshire cross-platform indigo", new DateTime(1328, 2, 15, 18, 48, 52, 711, DateTimeKind.Unspecified).AddTicks(1440), "Courts", 13, 18, 1 },
                    { 185, new DateTime(546, 9, 26, 11, 39, 48, 650, DateTimeKind.Unspecified).AddTicks(2000), "Ergonomic ROI envisioneer", new DateTime(1461, 4, 12, 6, 25, 40, 937, DateTimeKind.Unspecified).AddTicks(8192), "Practical Granite Shirt", 34, 18, 4 },
                    { 169, new DateTime(1905, 10, 1, 11, 13, 4, 651, DateTimeKind.Unspecified).AddTicks(6944), "payment Iranian Rial extend", new DateTime(1788, 8, 29, 1, 45, 25, 58, DateTimeKind.Unspecified).AddTicks(3248), "modular", 36, 18, 3 },
                    { 165, new DateTime(1599, 9, 18, 12, 45, 43, 904, DateTimeKind.Unspecified).AddTicks(9552), "Pa'anga Assimilated plum", new DateTime(1644, 12, 9, 12, 47, 27, 64, DateTimeKind.Unspecified).AddTicks(8336), "redundant", 38, 18, 1 },
                    { 244, new DateTime(59, 8, 30, 15, 46, 36, 586, DateTimeKind.Unspecified).AddTicks(7464), "back-end programming auxiliary", new DateTime(1011, 7, 27, 11, 9, 43, 445, DateTimeKind.Unspecified).AddTicks(2240), "Handmade Wooden Ball", 89, 18, 3 },
                    { 145, new DateTime(305, 7, 5, 17, 23, 14, 184, DateTimeKind.Unspecified).AddTicks(8832), "Central Locks synthesizing", new DateTime(969, 1, 11, 18, 50, 56, 897, DateTimeKind.Unspecified).AddTicks(768), "Graphical User Interface", 97, 18, 4 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 69, new DateTime(58, 10, 24, 5, 12, 55, 430, DateTimeKind.Unspecified).AddTicks(4956), "Berkshire Finland Rustic Wooden Chair", new DateTime(283, 7, 23, 21, 51, 57, 970, DateTimeKind.Unspecified).AddTicks(1152), "Freeway", 100, 18, 3 },
                    { 64, new DateTime(944, 5, 28, 3, 31, 33, 59, DateTimeKind.Unspecified).AddTicks(3104), "Bedfordshire Legacy redundant", new DateTime(341, 3, 7, 12, 15, 29, 862, DateTimeKind.Unspecified).AddTicks(9440), "moratorium", 98, 18, 4 },
                    { 46, new DateTime(328, 1, 30, 16, 13, 55, 600, DateTimeKind.Unspecified).AddTicks(560), "deposit Forge Sleek", new DateTime(348, 11, 18, 1, 3, 13, 625, DateTimeKind.Unspecified).AddTicks(4608), "Avon", 16, 18, 2 },
                    { 45, new DateTime(1938, 9, 9, 2, 17, 20, 437, DateTimeKind.Unspecified).AddTicks(9344), "Unbranded card Jewelery", new DateTime(82, 6, 29, 3, 28, 40, 720, DateTimeKind.Unspecified).AddTicks(7556), "Fantastic", 62, 18, 3 },
                    { 39, new DateTime(1521, 11, 17, 23, 21, 31, 536, DateTimeKind.Unspecified).AddTicks(1296), "Executive Tasty Concrete Cheese Rustic Granite Cheese", new DateTime(996, 6, 1, 6, 6, 14, 570, DateTimeKind.Unspecified).AddTicks(3888), "Fantastic Cotton Chicken", 81, 18, 2 },
                    { 289, new DateTime(1469, 9, 22, 17, 14, 29, 506, DateTimeKind.Unspecified).AddTicks(560), "Unbranded Fresh Keyboard Savings Account monitor", new DateTime(15, 4, 25, 20, 24, 11, 670, DateTimeKind.Unspecified).AddTicks(2796), "Global", 73, 13, 1 },
                    { 123, new DateTime(278, 3, 7, 18, 43, 55, 641, DateTimeKind.Unspecified).AddTicks(8640), "incubate extend Health", new DateTime(112, 12, 23, 6, 0, 44, 435, DateTimeKind.Unspecified).AddTicks(6708), "primary", 23, 18, 4 },
                    { 245, new DateTime(1067, 5, 18, 20, 9, 56, 567, DateTimeKind.Unspecified).AddTicks(6816), "redefine Bedfordshire monitor", new DateTime(1868, 11, 18, 8, 20, 12, 833, DateTimeKind.Unspecified).AddTicks(7104), "Village", 10, 18, 3 },
                    { 256, new DateTime(151, 8, 13, 17, 22, 16, 583, DateTimeKind.Unspecified).AddTicks(7712), "Fantastic Fresh Shirt engineer AI", new DateTime(731, 1, 28, 11, 40, 7, 24, DateTimeKind.Unspecified).AddTicks(1808), "Dominican Peso", 47, 18, 1 },
                    { 263, new DateTime(1684, 4, 6, 6, 53, 17, 921, DateTimeKind.Unspecified).AddTicks(7488), "Plastic content Bedfordshire", new DateTime(246, 9, 9, 2, 8, 55, 489, DateTimeKind.Unspecified).AddTicks(2352), "Liaison", 82, 18, 3 },
                    { 171, new DateTime(173, 12, 28, 1, 8, 24, 286, DateTimeKind.Unspecified).AddTicks(5936), "hacking expedite actuating", new DateTime(1005, 11, 16, 18, 10, 40, 685, DateTimeKind.Unspecified).AddTicks(5824), "primary", 23, 10, 1 },
                    { 114, new DateTime(1931, 2, 12, 23, 8, 35, 542, DateTimeKind.Unspecified).AddTicks(9584), "platforms Anguilla Generic", new DateTime(1701, 11, 9, 22, 20, 38, 90, DateTimeKind.Unspecified).AddTicks(9968), "paradigms", 12, 10, 4 },
                    { 102, new DateTime(1263, 1, 21, 22, 2, 0, 308, DateTimeKind.Unspecified).AddTicks(4112), "red North Carolina tertiary", new DateTime(588, 12, 15, 0, 58, 34, 363, DateTimeKind.Unspecified).AddTicks(5344), "Credit Card Account", 90, 10, 4 },
                    { 98, new DateTime(1321, 10, 31, 12, 18, 54, 40, DateTimeKind.Unspecified).AddTicks(5008), "payment Ridges Rustic Plastic Car", new DateTime(815, 2, 26, 19, 53, 48, 688, DateTimeKind.Unspecified).AddTicks(2224), "Fresh", 76, 10, 2 },
                    { 91, new DateTime(1906, 3, 22, 5, 6, 5, 755, DateTimeKind.Unspecified).AddTicks(2208), "Tunnel Research strategy", new DateTime(685, 3, 18, 19, 19, 43, 539, DateTimeKind.Unspecified).AddTicks(3296), "multi-byte", 44, 10, 3 },
                    { 87, new DateTime(688, 7, 30, 19, 31, 54, 520, DateTimeKind.Unspecified).AddTicks(7248), "Pitcairn Islands Reactive black", new DateTime(936, 3, 8, 7, 33, 57, 987, DateTimeKind.Unspecified).AddTicks(3936), "Northern Mariana Islands", 46, 10, 4 },
                    { 84, new DateTime(1215, 10, 27, 13, 51, 35, 955, DateTimeKind.Unspecified).AddTicks(3744), "withdrawal copying Home Loan Account", new DateTime(1775, 3, 29, 4, 16, 34, 194, DateTimeKind.Unspecified).AddTicks(6832), "JBOD", 22, 10, 3 },
                    { 78, new DateTime(1952, 2, 25, 22, 52, 48, 910, DateTimeKind.Unspecified).AddTicks(2032), "haptic online Bedfordshire", new DateTime(130, 8, 19, 20, 11, 19, 312, DateTimeKind.Unspecified).AddTicks(8), "Movies & Shoes", 88, 10, 3 },
                    { 73, new DateTime(1022, 11, 27, 2, 50, 50, 591, DateTimeKind.Unspecified).AddTicks(2400), "strategize HTTP Avon", new DateTime(982, 3, 24, 11, 37, 53, 885, DateTimeKind.Unspecified).AddTicks(6784), "override", 86, 10, 2 },
                    { 65, new DateTime(1350, 10, 10, 2, 37, 8, 899, DateTimeKind.Unspecified).AddTicks(4512), "content Kids monitor", new DateTime(649, 1, 16, 13, 50, 24, 776, DateTimeKind.Unspecified).AddTicks(5744), "Auto Loan Account", 50, 10, 3 },
                    { 59, new DateTime(1866, 10, 16, 12, 32, 40, 200, DateTimeKind.Unspecified).AddTicks(8656), "Mauritius Rupee GB models", new DateTime(485, 2, 21, 23, 44, 54, 493, DateTimeKind.Unspecified).AddTicks(8512), "Dynamic", 18, 10, 3 },
                    { 56, new DateTime(295, 6, 19, 11, 26, 29, 227, DateTimeKind.Unspecified).AddTicks(1696), "Freeway Ford Square", new DateTime(716, 8, 13, 19, 7, 45, 445, DateTimeKind.Unspecified).AddTicks(8288), "Handcrafted", 83, 10, 2 },
                    { 49, new DateTime(734, 6, 3, 23, 28, 56, 964, DateTimeKind.Unspecified).AddTicks(8176), "Avenue International program", new DateTime(1739, 9, 21, 19, 15, 22, 320, DateTimeKind.Unspecified).AddTicks(3024), "payment", 62, 10, 3 },
                    { 3, new DateTime(1614, 6, 3, 17, 22, 12, 832, DateTimeKind.Unspecified).AddTicks(528), "secured line Legacy wireless", new DateTime(961, 3, 16, 22, 54, 32, 302, DateTimeKind.Unspecified).AddTicks(6960), "Cape Verde Escudo", 74, 10, 1 },
                    { 270, new DateTime(314, 1, 31, 23, 6, 29, 285, DateTimeKind.Unspecified).AddTicks(8752), "Direct intuitive Adaptive", new DateTime(579, 2, 5, 13, 33, 38, 348, DateTimeKind.Unspecified).AddTicks(528), "Checking Account", 79, 18, 1 },
                    { 236, new DateTime(1325, 6, 25, 2, 1, 51, 378, DateTimeKind.Unspecified).AddTicks(5744), "Plastic RSS compressing", new DateTime(1619, 7, 30, 21, 56, 2, 486, DateTimeKind.Unspecified).AddTicks(7792), "proactive", 29, 13, 3 },
                    { 209, new DateTime(282, 5, 15, 19, 37, 40, 718, DateTimeKind.Unspecified).AddTicks(7520), "Avon Serbian Dinar hack", new DateTime(954, 3, 26, 3, 42, 17, 208, DateTimeKind.Unspecified).AddTicks(9680), "client-server", 63, 13, 3 },
                    { 183, new DateTime(1041, 3, 12, 20, 46, 38, 477, DateTimeKind.Unspecified).AddTicks(2176), "Home, Tools & Computers SAS indigo", new DateTime(474, 11, 23, 20, 16, 17, 156, DateTimeKind.Unspecified).AddTicks(8784), "invoice", 49, 13, 4 },
                    { 149, new DateTime(1965, 10, 19, 1, 49, 5, 967, DateTimeKind.Unspecified).AddTicks(7392), "Auto Loan Account withdrawal SSL", new DateTime(31, 11, 7, 23, 57, 17, 309, DateTimeKind.Unspecified).AddTicks(2438), "Sleek Wooden Ball", 9, 13, 1 },
                    { 283, new DateTime(1647, 8, 14, 22, 22, 12, 224, DateTimeKind.Unspecified).AddTicks(8592), "mobile Dynamic Michigan", new DateTime(1387, 4, 4, 5, 2, 9, 942, DateTimeKind.Unspecified).AddTicks(2544), "Home & Clothing", 25, 1, 4 },
                    { 260, new DateTime(1070, 5, 17, 15, 24, 4, 505, DateTimeKind.Unspecified).AddTicks(6784), "workforce Checking Account Stravenue", new DateTime(1772, 9, 23, 4, 23, 2, 311, DateTimeKind.Unspecified).AddTicks(6816), "Home Loan Account", 60, 1, 4 },
                    { 250, new DateTime(381, 1, 14, 10, 57, 19, 55, DateTimeKind.Unspecified).AddTicks(9584), "Maryland Rubber RSS", new DateTime(1286, 2, 14, 12, 38, 42, 310, DateTimeKind.Unspecified).AddTicks(8944), "Montana", 43, 1, 4 },
                    { 233, new DateTime(1438, 2, 4, 10, 15, 39, 537, DateTimeKind.Unspecified).AddTicks(4480), "Books, Electronics & Movies reboot Books & Baby", new DateTime(1697, 10, 15, 3, 18, 26, 31, DateTimeKind.Unspecified).AddTicks(7840), "SDD", 36, 1, 1 },
                    { 159, new DateTime(288, 9, 24, 8, 3, 20, 631, DateTimeKind.Unspecified).AddTicks(8128), "Branding purple empower", new DateTime(1848, 3, 20, 8, 24, 40, 556, DateTimeKind.Unspecified).AddTicks(9744), "Borders", 13, 1, 3 },
                    { 151, new DateTime(1060, 9, 5, 17, 52, 29, 117, DateTimeKind.Unspecified).AddTicks(3904), "Checking Account proactive Bedfordshire", new DateTime(1652, 12, 23, 5, 28, 56, 573, DateTimeKind.Unspecified).AddTicks(8000), "context-sensitive", 40, 1, 2 },
                    { 130, new DateTime(1547, 9, 2, 21, 6, 27, 622, DateTimeKind.Unspecified).AddTicks(6704), "Legacy repurpose orange", new DateTime(598, 10, 9, 12, 10, 27, 838, DateTimeKind.Unspecified).AddTicks(9264), "Intelligent", 5, 1, 3 },
                    { 124, new DateTime(1580, 10, 18, 1, 51, 53, 716, DateTimeKind.Unspecified).AddTicks(7952), "Bedfordshire repurpose Money Market Account", new DateTime(844, 10, 18, 2, 38, 31, 879, DateTimeKind.Unspecified).AddTicks(7328), "Outdoors", 12, 1, 4 },
                    { 104, new DateTime(706, 2, 12, 23, 45, 50, 64, DateTimeKind.Unspecified).AddTicks(7376), "Refined Granite Keyboard Assimilated PCI", new DateTime(1082, 7, 8, 14, 40, 6, 310, DateTimeKind.Unspecified).AddTicks(624), "payment", 76, 1, 1 },
                    { 68, new DateTime(1428, 6, 9, 18, 23, 53, 359, DateTimeKind.Unspecified).AddTicks(8736), "Representative Kids granular", new DateTime(868, 12, 7, 19, 44, 34, 668, DateTimeKind.Unspecified).AddTicks(7792), "Illinois", 34, 1, 2 },
                    { 54, new DateTime(1771, 8, 12, 13, 55, 47, 431, DateTimeKind.Unspecified).AddTicks(416), "system-worthy portals wireless", new DateTime(1288, 1, 25, 18, 17, 2, 148, DateTimeKind.Unspecified).AddTicks(336), "Investment Account", 22, 1, 1 },
                    { 47, new DateTime(605, 1, 21, 16, 6, 16, 129, DateTimeKind.Unspecified).AddTicks(3520), "Usability Executive Village", new DateTime(591, 8, 18, 9, 8, 21, 771, DateTimeKind.Unspecified).AddTicks(6240), "transition", 10, 1, 3 },
                    { 42, new DateTime(430, 7, 4, 7, 49, 34, 390, DateTimeKind.Unspecified).AddTicks(416), "applications Antigua and Barbuda non-volatile", new DateTime(612, 12, 25, 19, 32, 21, 210, DateTimeKind.Unspecified).AddTicks(5360), "grey", 8, 1, 3 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 287, new DateTime(279, 10, 22, 8, 10, 50, 995, DateTimeKind.Unspecified).AddTicks(3040), "archive Quality XSS", new DateTime(1192, 4, 9, 12, 39, 53, 120, DateTimeKind.Unspecified).AddTicks(2064), "Soft", 79, 2, 2 },
                    { 243, new DateTime(1089, 4, 30, 4, 26, 27, 627, DateTimeKind.Unspecified).AddTicks(7456), "connecting Technician Cross-group", new DateTime(99, 12, 31, 5, 36, 21, 328, DateTimeKind.Unspecified).AddTicks(4132), "SDD", 21, 2, 3 },
                    { 286, new DateTime(785, 5, 2, 21, 22, 25, 564, DateTimeKind.Unspecified).AddTicks(6384), "Awesome Soft Chair Implementation programming", new DateTime(141, 5, 26, 14, 9, 55, 728, DateTimeKind.Unspecified).AddTicks(6112), "SMS", 43, 1, 4 },
                    { 189, new DateTime(350, 9, 13, 18, 30, 26, 467, DateTimeKind.Unspecified).AddTicks(2160), "Investment Account Legacy Georgia", new DateTime(195, 2, 6, 0, 23, 2, 480, DateTimeKind.Unspecified).AddTicks(6192), "Ergonomic Fresh Salad", 35, 2, 4 },
                    { 299, new DateTime(177, 4, 21, 22, 37, 25, 189, DateTimeKind.Unspecified).AddTicks(8552), "pixel application Open-architected", new DateTime(641, 10, 19, 13, 59, 8, 888, DateTimeKind.Unspecified).AddTicks(7632), "Springs", 82, 1, 2 },
                    { 34, new DateTime(1168, 9, 16, 4, 53, 36, 627, DateTimeKind.Unspecified).AddTicks(7136), "eco-centric THX navigating", new DateTime(494, 11, 8, 0, 9, 48, 29, DateTimeKind.Unspecified).AddTicks(5216), "internet solution", 20, 12, 2 },
                    { 109, new DateTime(702, 4, 2, 21, 9, 41, 574, DateTimeKind.Unspecified).AddTicks(9840), "payment deposit Quality", new DateTime(702, 2, 11, 16, 45, 17, 398, DateTimeKind.Unspecified).AddTicks(1968), "CSS", 35, 13, 1 },
                    { 105, new DateTime(305, 2, 1, 20, 14, 6, 844, DateTimeKind.Unspecified).AddTicks(2672), "optical Optimized Comoro Franc", new DateTime(1477, 8, 13, 7, 54, 29, 181, DateTimeKind.Unspecified).AddTicks(640), "Checking Account", 5, 13, 3 },
                    { 58, new DateTime(746, 2, 5, 21, 32, 44, 45, DateTimeKind.Unspecified).AddTicks(704), "Assurance Principal Investment Account", new DateTime(622, 11, 12, 18, 50, 21, 371, DateTimeKind.Unspecified).AddTicks(1664), "infrastructures", 66, 13, 2 },
                    { 48, new DateTime(574, 9, 1, 13, 38, 34, 188, DateTimeKind.Unspecified).AddTicks(1200), "challenge teal Intelligent", new DateTime(181, 11, 2, 21, 59, 57, 641, DateTimeKind.Unspecified).AddTicks(232), "Administrator", 27, 13, 3 },
                    { 30, new DateTime(1531, 4, 13, 16, 1, 31, 494, DateTimeKind.Unspecified).AddTicks(3248), "secondary hub Legacy", new DateTime(1593, 10, 21, 3, 44, 54, 626, DateTimeKind.Unspecified).AddTicks(6640), "sky blue", 93, 13, 1 },
                    { 284, new DateTime(1316, 9, 25, 22, 27, 46, 493, DateTimeKind.Unspecified).AddTicks(5632), "transmit Refined Frozen Ball Borders", new DateTime(1657, 4, 24, 0, 24, 8, 583, DateTimeKind.Unspecified).AddTicks(4000), "invoice", 92, 12, 4 },
                    { 268, new DateTime(34, 1, 8, 2, 50, 32, 712, DateTimeKind.Unspecified).AddTicks(7548), "Shoes, Tools & Beauty mint green standardization", new DateTime(1327, 10, 22, 21, 23, 4, 34, DateTimeKind.Unspecified).AddTicks(3952), "microchip", 37, 12, 2 },
                    { 184, new DateTime(1040, 2, 14, 18, 1, 27, 318, DateTimeKind.Unspecified).AddTicks(1264), "hacking challenge Quality", new DateTime(1605, 9, 8, 11, 4, 26, 240, DateTimeKind.Unspecified).AddTicks(9936), "disintermediate", 44, 12, 1 },
                    { 143, new DateTime(634, 10, 29, 5, 39, 17, 239, DateTimeKind.Unspecified).AddTicks(4256), "lime evolve indexing", new DateTime(64, 3, 20, 9, 39, 17, 129, DateTimeKind.Unspecified).AddTicks(1156), "monetize", 65, 12, 4 },
                    { 129, new DateTime(15, 4, 22, 15, 22, 30, 951, DateTimeKind.Unspecified).AddTicks(6186), "explicit Steel Clothing, Industrial & Clothing", new DateTime(1603, 2, 2, 2, 45, 51, 22, DateTimeKind.Unspecified).AddTicks(3952), "Gorgeous Rubber Mouse", 59, 12, 2 },
                    { 97, new DateTime(987, 9, 5, 7, 59, 40, 7, DateTimeKind.Unspecified).AddTicks(4192), "iterate feed digital", new DateTime(712, 12, 13, 14, 55, 8, 542, DateTimeKind.Unspecified).AddTicks(3792), "index", 26, 12, 4 },
                    { 79, new DateTime(1899, 3, 9, 11, 16, 51, 545, DateTimeKind.Unspecified).AddTicks(1472), "Diverse sensor Rustic Rubber Chips", new DateTime(1274, 11, 15, 0, 30, 11, 565, DateTimeKind.Unspecified).AddTicks(3584), "white", 47, 12, 3 },
                    { 70, new DateTime(150, 7, 19, 5, 55, 12, 50, DateTimeKind.Unspecified).AddTicks(2520), "Lead online sensor", new DateTime(216, 8, 13, 17, 1, 31, 459, DateTimeKind.Unspecified).AddTicks(4624), "index", 94, 12, 2 },
                    { 66, new DateTime(961, 5, 8, 3, 22, 13, 875, DateTimeKind.Unspecified).AddTicks(6112), "Practical Wooden Pizza Incredible Granite Fish Movies", new DateTime(516, 11, 8, 15, 2, 42, 704, DateTimeKind.Unspecified).AddTicks(624), "Massachusetts", 18, 12, 4 },
                    { 60, new DateTime(1274, 12, 14, 6, 57, 12, 556, DateTimeKind.Unspecified).AddTicks(2064), "Fantastic Rubber Hat Personal Loan Account seize", new DateTime(514, 5, 27, 3, 10, 36, 261, DateTimeKind.Unspecified).AddTicks(1792), "index", 30, 12, 1 },
                    { 16, new DateTime(1268, 11, 5, 10, 57, 50, 30, DateTimeKind.Unspecified).AddTicks(4080), "deposit overriding Bedfordshire", new DateTime(1799, 3, 23, 12, 1, 15, 127, DateTimeKind.Unspecified).AddTicks(288), "auxiliary", 93, 12, 1 },
                    { 177, new DateTime(385, 12, 3, 17, 56, 25, 313, DateTimeKind.Unspecified).AddTicks(7376), "Tools & Tools Soft Specialist", new DateTime(1388, 3, 19, 17, 23, 43, 443, DateTimeKind.Unspecified).AddTicks(352), "Consultant", 4, 9, 4 },
                    { 164, new DateTime(783, 12, 4, 0, 21, 12, 785, DateTimeKind.Unspecified).AddTicks(2080), "Mozambique Wooden copy", new DateTime(1296, 8, 1, 19, 21, 57, 773, DateTimeKind.Unspecified).AddTicks(1472), "portal", 66, 9, 2 },
                    { 158, new DateTime(1099, 10, 3, 4, 20, 30, 752, DateTimeKind.Unspecified).AddTicks(7568), "turquoise card Berkshire", new DateTime(40, 2, 9, 10, 8, 39, 780, DateTimeKind.Unspecified).AddTicks(5944), "Saint Vincent and the Grenadines", 49, 9, 1 },
                    { 99, new DateTime(1633, 7, 10, 20, 37, 11, 393, DateTimeKind.Unspecified).AddTicks(7168), "Berkshire Concrete Books & Garden", new DateTime(856, 10, 12, 9, 28, 35, 228, DateTimeKind.Unspecified).AddTicks(7376), "synthesize", 93, 4, 1 },
                    { 90, new DateTime(1380, 1, 27, 11, 0, 54, 653, DateTimeKind.Unspecified).AddTicks(5632), "Handmade secured line navigating", new DateTime(763, 2, 9, 17, 44, 0, 971, DateTimeKind.Unspecified).AddTicks(3328), "Future", 33, 4, 4 },
                    { 77, new DateTime(722, 10, 3, 19, 30, 24, 316, DateTimeKind.Unspecified).AddTicks(2256), "Down-sized Front-line sky blue", new DateTime(1476, 2, 11, 2, 36, 43, 201, DateTimeKind.Unspecified).AddTicks(1792), "Key", 14, 4, 2 },
                    { 57, new DateTime(1432, 9, 11, 1, 16, 52, 822, DateTimeKind.Unspecified).AddTicks(2608), "Djibouti Boliviano boliviano Small Metal Hat", new DateTime(1829, 5, 24, 21, 17, 52, 672, DateTimeKind.Unspecified).AddTicks(8400), "black", 95, 4, 1 },
                    { 51, new DateTime(56, 11, 23, 0, 39, 3, 387, DateTimeKind.Unspecified).AddTicks(5924), "SMTP Streets Guyana", new DateTime(684, 1, 24, 8, 38, 44, 748, DateTimeKind.Unspecified).AddTicks(2064), "Representative", 98, 4, 2 },
                    { 41, new DateTime(630, 6, 5, 0, 18, 3, 16, DateTimeKind.Unspecified).AddTicks(8912), "envisioneer Trinidad and Tobago Dollar Locks", new DateTime(19, 1, 21, 3, 35, 36, 731, DateTimeKind.Unspecified).AddTicks(4895), "Mexican Peso", 34, 4, 2 },
                    { 113, new DateTime(1426, 2, 5, 16, 53, 21, 703, DateTimeKind.Unspecified).AddTicks(5856), "Specialist Niue Tasty Rubber Sausages", new DateTime(1637, 10, 26, 12, 35, 25, 203, DateTimeKind.Unspecified).AddTicks(5536), "neural", 62, 4, 4 },
                    { 33, new DateTime(647, 11, 9, 20, 2, 0, 29, DateTimeKind.Unspecified).AddTicks(5568), "Credit Card Account protocol Enhanced", new DateTime(1252, 3, 13, 21, 17, 55, 329, DateTimeKind.Unspecified).AddTicks(8448), "Refined Fresh Keyboard", 90, 4, 2 },
                    { 24, new DateTime(1263, 8, 26, 3, 37, 31, 107, DateTimeKind.Unspecified).AddTicks(9184), "1080p Computers & Home Ergonomic Frozen Shoes", new DateTime(1805, 3, 10, 5, 56, 28, 825, DateTimeKind.Unspecified).AddTicks(8576), "Borders", 46, 4, 4 },
                    { 20, new DateTime(44, 4, 25, 13, 14, 27, 479, DateTimeKind.Unspecified).AddTicks(9424), "Streamlined Awesome Steel Sausages Plain", new DateTime(1326, 5, 22, 19, 1, 16, 720, DateTimeKind.Unspecified).AddTicks(848), "Cambridgeshire", 47, 4, 1 },
                    { 15, new DateTime(1646, 10, 15, 15, 26, 41, 337, DateTimeKind.Unspecified).AddTicks(6208), "transmitting utilize mobile", new DateTime(1243, 8, 28, 6, 54, 52, 559, DateTimeKind.Unspecified).AddTicks(7904), "turn-key", 72, 4, 4 },
                    { 279, new DateTime(966, 2, 21, 16, 6, 20, 692, DateTimeKind.Unspecified).AddTicks(7184), "Branding Cotton Money Market Account", new DateTime(1158, 7, 2, 6, 36, 35, 652, DateTimeKind.Unspecified).AddTicks(2384), "24/7", 25, 19, 1 },
                    { 278, new DateTime(686, 12, 6, 4, 27, 45, 981, DateTimeKind.Unspecified).AddTicks(7360), "Corporate Faroe Islands Organized", new DateTime(1812, 11, 27, 12, 7, 36, 547, DateTimeKind.Unspecified).AddTicks(8544), "Soft", 81, 19, 4 },
                    { 273, new DateTime(576, 4, 20, 16, 11, 45, 702, DateTimeKind.Unspecified).AddTicks(5968), "Tanzanian Shilling XSS IB", new DateTime(1070, 3, 26, 20, 51, 21, 279, DateTimeKind.Unspecified).AddTicks(7200), "Kip", 3, 19, 1 },
                    { 32, new DateTime(1282, 2, 16, 21, 42, 7, 789, DateTimeKind.Unspecified).AddTicks(9152), "Regional Frozen Intelligent Metal Car", new DateTime(1685, 3, 4, 20, 0, 29, 125, DateTimeKind.Unspecified).AddTicks(192), "HDD", 60, 4, 4 },
                    { 121, new DateTime(1976, 1, 29, 3, 14, 29, 882, DateTimeKind.Unspecified).AddTicks(7088), "Metal Kids Coordinator", new DateTime(783, 11, 8, 21, 17, 17, 138, DateTimeKind.Unspecified).AddTicks(3024), "Incredible", 77, 4, 4 },
                    { 126, new DateTime(1683, 10, 27, 2, 49, 5, 308, DateTimeKind.Unspecified).AddTicks(2256), "interfaces granular Investment Account", new DateTime(287, 5, 15, 19, 15, 2, 796, DateTimeKind.Unspecified).AddTicks(1696), "Vanuatu", 4, 4, 3 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 134, new DateTime(1642, 7, 1, 17, 23, 58, 475, DateTimeKind.Unspecified).AddTicks(3424), "Electronics, Home & Baby virtual PNG", new DateTime(1686, 5, 22, 20, 57, 10, 523, DateTimeKind.Unspecified).AddTicks(2464), "Personal Loan Account", 90, 4, 2 },
                    { 118, new DateTime(276, 6, 20, 19, 15, 20, 375, DateTimeKind.Unspecified).AddTicks(8032), "Movies enhance mission-critical", new DateTime(246, 1, 13, 7, 44, 38, 630, DateTimeKind.Unspecified).AddTicks(5408), "New Zealand Dollar", 39, 17, 1 },
                    { 76, new DateTime(1776, 2, 23, 1, 40, 53, 817, DateTimeKind.Unspecified).AddTicks(256), "Generic XML hack", new DateTime(152, 7, 25, 1, 1, 32, 880, DateTimeKind.Unspecified).AddTicks(9624), "Frozen", 84, 17, 2 },
                    { 8, new DateTime(43, 3, 25, 12, 19, 35, 771, DateTimeKind.Unspecified).AddTicks(604), "Beauty Small Wooden Salad evolve", new DateTime(484, 11, 11, 17, 10, 54, 799, DateTimeKind.Unspecified).AddTicks(7744), "Phased", 60, 17, 4 },
                    { 298, new DateTime(1037, 12, 3, 1, 33, 55, 196, DateTimeKind.Unspecified).AddTicks(5072), "Home Loan Account payment invoice", new DateTime(1588, 9, 25, 17, 44, 4, 81, DateTimeKind.Unspecified).AddTicks(5312), "indexing", 26, 4, 4 },
                    { 294, new DateTime(1126, 12, 29, 3, 35, 22, 653, DateTimeKind.Unspecified).AddTicks(6272), "User-friendly Division firewall", new DateTime(1036, 5, 30, 15, 19, 20, 240, DateTimeKind.Unspecified).AddTicks(3408), "1080p", 83, 4, 3 },
                    { 271, new DateTime(1405, 7, 21, 10, 22, 26, 467, DateTimeKind.Unspecified).AddTicks(1440), "Small Soft Hat Avon 1080p", new DateTime(378, 1, 23, 3, 36, 20, 511, DateTimeKind.Unspecified).AddTicks(8576), "reboot", 8, 4, 2 },
                    { 266, new DateTime(1496, 10, 25, 6, 47, 55, 838, DateTimeKind.Unspecified).AddTicks(1584), "Investor stable Canada", new DateTime(863, 6, 12, 21, 14, 20, 862, DateTimeKind.Unspecified).AddTicks(3568), "Health, Kids & Baby", 39, 4, 1 },
                    { 255, new DateTime(1575, 2, 16, 3, 22, 59, 48, DateTimeKind.Unspecified).AddTicks(9296), "Bedfordshire syndicate quantifying", new DateTime(322, 10, 3, 16, 55, 52, 731, DateTimeKind.Unspecified).AddTicks(8368), "Namibia", 93, 4, 4 },
                    { 238, new DateTime(1407, 7, 27, 12, 35, 33, 191, DateTimeKind.Unspecified).AddTicks(3872), "ROI Germany synergize", new DateTime(844, 2, 12, 1, 50, 31, 291, DateTimeKind.Unspecified).AddTicks(6688), "Mandatory", 12, 4, 4 },
                    { 219, new DateTime(1744, 7, 13, 0, 21, 35, 653, DateTimeKind.Unspecified).AddTicks(5888), "Frozen monitor withdrawal", new DateTime(69, 3, 17, 10, 38, 5, 609, DateTimeKind.Unspecified).AddTicks(7464), "Handmade Rubber Chips", 1, 4, 3 },
                    { 196, new DateTime(991, 12, 7, 4, 35, 24, 662, DateTimeKind.Unspecified).AddTicks(7344), "Pataca strategic scalable", new DateTime(399, 12, 27, 23, 44, 56, 453, DateTimeKind.Unspecified).AddTicks(2560), "Station", 93, 4, 1 },
                    { 188, new DateTime(1353, 3, 5, 17, 0, 53, 907, DateTimeKind.Unspecified).AddTicks(2528), "HDD mindshare West Virginia", new DateTime(1422, 5, 16, 22, 49, 31, 402, DateTimeKind.Unspecified).AddTicks(9264), "Small", 100, 4, 4 },
                    { 181, new DateTime(1176, 9, 3, 2, 25, 3, 49, DateTimeKind.Unspecified).AddTicks(6784), "driver South Carolina incentivize", new DateTime(382, 9, 4, 2, 30, 36, 389, DateTimeKind.Unspecified).AddTicks(1168), "Music", 64, 4, 2 },
                    { 174, new DateTime(1850, 3, 29, 12, 16, 1, 660, DateTimeKind.Unspecified).AddTicks(1808), "Jewelery & Kids calculating solid state", new DateTime(1066, 7, 24, 1, 53, 52, 840, DateTimeKind.Unspecified).AddTicks(5264), "Handcrafted Soft Chair", 92, 4, 3 },
                    { 137, new DateTime(686, 4, 7, 15, 4, 17, 486, DateTimeKind.Unspecified).AddTicks(8208), "hack redundant SSL", new DateTime(1568, 11, 11, 9, 52, 43, 406, DateTimeKind.Unspecified).AddTicks(8688), "Washington", 44, 4, 3 },
                    { 265, new DateTime(794, 3, 29, 18, 24, 16, 47, DateTimeKind.Unspecified).AddTicks(128), "Horizontal Village Mexico", new DateTime(583, 11, 14, 2, 46, 10, 307, DateTimeKind.Unspecified).AddTicks(1856), "Incredible Granite Bike", 79, 19, 2 },
                    { 257, new DateTime(1598, 10, 14, 22, 24, 19, 627, DateTimeKind.Unspecified).AddTicks(800), "purple secured line Frozen", new DateTime(1766, 1, 1, 18, 0, 22, 811, DateTimeKind.Unspecified).AddTicks(5536), "Concrete", 78, 19, 1 },
                    { 232, new DateTime(1648, 10, 14, 1, 17, 17, 96, DateTimeKind.Unspecified).AddTicks(7376), "Executive multi-byte Glens", new DateTime(1189, 11, 17, 23, 25, 34, 693, DateTimeKind.Unspecified).AddTicks(3136), "Kansas", 2, 19, 1 },
                    { 229, new DateTime(617, 4, 16, 16, 51, 28, 756, DateTimeKind.Unspecified).AddTicks(1392), "cross-platform Marketing Rustic", new DateTime(281, 5, 17, 12, 49, 23, 566, DateTimeKind.Unspecified).AddTicks(80), "Gorgeous Rubber Shirt", 44, 19, 2 },
                    { 52, new DateTime(1561, 5, 22, 11, 2, 45, 634, DateTimeKind.Unspecified).AddTicks(5040), "Fantastic Steel Table Falls real-time", new DateTime(1078, 10, 1, 16, 32, 33, 676, DateTimeKind.Unspecified).AddTicks(9616), "Utah", 39, 11, 1 },
                    { 21, new DateTime(1663, 5, 27, 11, 12, 32, 528, DateTimeKind.Unspecified).AddTicks(8400), "invoice Yemeni Rial Executive", new DateTime(1139, 11, 9, 21, 0, 8, 216, DateTimeKind.Unspecified).AddTicks(2000), "scale", 47, 11, 2 },
                    { 7, new DateTime(1437, 11, 21, 5, 57, 53, 875, DateTimeKind.Unspecified).AddTicks(6880), "Global Health & Baby architectures", new DateTime(931, 8, 15, 15, 38, 22, 184, DateTimeKind.Unspecified).AddTicks(8720), "open-source", 33, 11, 2 },
                    { 300, new DateTime(666, 1, 6, 7, 15, 36, 884, DateTimeKind.Unspecified).AddTicks(6032), "Home & Health Balanced Fields", new DateTime(1132, 5, 24, 19, 46, 41, 153, DateTimeKind.Unspecified).AddTicks(4480), "Unbranded Concrete Table", 98, 6, 4 },
                    { 285, new DateTime(124, 12, 22, 6, 38, 7, 798, DateTimeKind.Unspecified).AddTicks(9600), "real-time Strategist Accounts", new DateTime(1628, 3, 21, 11, 10, 6, 311, DateTimeKind.Unspecified).AddTicks(3616), "Stream", 85, 6, 1 },
                    { 264, new DateTime(1346, 3, 15, 10, 55, 40, 420, DateTimeKind.Unspecified).AddTicks(784), "Generic adapter Cotton", new DateTime(1587, 1, 15, 12, 20, 3, 49, DateTimeKind.Unspecified).AddTicks(8768), "Unbranded", 25, 6, 2 },
                    { 231, new DateTime(236, 3, 1, 10, 41, 25, 334, DateTimeKind.Unspecified).AddTicks(9952), "Bedfordshire Personal Loan Account payment", new DateTime(1734, 8, 19, 13, 53, 24, 674, DateTimeKind.Unspecified).AddTicks(176), "blue", 19, 6, 3 },
                    { 223, new DateTime(1686, 3, 18, 12, 42, 25, 891, DateTimeKind.Unspecified).AddTicks(1248), "Kenyan Shilling Small Wooden Towels Plastic", new DateTime(1778, 11, 15, 10, 37, 56, 460, DateTimeKind.Unspecified).AddTicks(1232), "Avon", 9, 6, 4 },
                    { 222, new DateTime(905, 1, 25, 12, 39, 40, 805, DateTimeKind.Unspecified).AddTicks(2848), "Horizontal Junctions interactive", new DateTime(1100, 5, 10, 4, 6, 18, 224, DateTimeKind.Unspecified).AddTicks(4688), "initiative", 97, 6, 3 },
                    { 176, new DateTime(236, 7, 19, 18, 45, 38, 703, DateTimeKind.Unspecified).AddTicks(3264), "Bedfordshire Intelligent Metal Hat leading edge", new DateTime(867, 7, 6, 0, 0, 31, 498, DateTimeKind.Unspecified).AddTicks(7312), "Advanced", 30, 6, 4 },
                    { 144, new DateTime(394, 4, 19, 12, 33, 22, 478, DateTimeKind.Unspecified).AddTicks(6336), "Saint Kitts and Nevis e-enable Program", new DateTime(1127, 3, 14, 15, 41, 53, 384, DateTimeKind.Unspecified).AddTicks(3728), "success", 51, 6, 2 },
                    { 141, new DateTime(29, 4, 28, 16, 44, 44, 97, DateTimeKind.Unspecified).AddTicks(2813), "Licensed Frozen Shirt Harbor Buckinghamshire", new DateTime(1253, 5, 18, 22, 41, 29, 318, DateTimeKind.Unspecified).AddTicks(7280), "Practical", 63, 6, 1 },
                    { 119, new DateTime(1680, 3, 13, 11, 26, 15, 656, DateTimeKind.Unspecified).AddTicks(3408), "Legacy Fresh intuitive", new DateTime(940, 10, 17, 16, 30, 13, 248, DateTimeKind.Unspecified).AddTicks(2576), "quantifying", 48, 6, 1 },
                    { 89, new DateTime(1470, 9, 29, 15, 4, 55, 868, DateTimeKind.Unspecified).AddTicks(2640), "Fantastic Wooden Sausages deposit synthesize", new DateTime(317, 5, 25, 11, 27, 38, 507, DateTimeKind.Unspecified).AddTicks(8752), "AI", 58, 6, 4 },
                    { 37, new DateTime(284, 12, 25, 0, 42, 17, 334, DateTimeKind.Unspecified).AddTicks(8784), "Maryland Interactions firewall", new DateTime(1758, 8, 14, 22, 1, 45, 914, DateTimeKind.Unspecified).AddTicks(8560), "Borders", 32, 6, 1 },
                    { 153, new DateTime(1280, 9, 29, 22, 25, 19, 111, DateTimeKind.Unspecified).AddTicks(96), "Checking Account Home Loan Account compressing", new DateTime(1182, 6, 24, 4, 13, 6, 859, DateTimeKind.Unspecified).AddTicks(3552), "copying", 21, 11, 1 },
                    { 142, new DateTime(737, 5, 12, 14, 10, 12, 117, DateTimeKind.Unspecified).AddTicks(1696), "Pines granular Pataca", new DateTime(972, 1, 28, 20, 42, 33, 96, DateTimeKind.Unspecified).AddTicks(6608), "withdrawal", 5, 17, 3 },
                    { 182, new DateTime(1341, 4, 15, 11, 35, 23, 589, DateTimeKind.Unspecified).AddTicks(8896), "Future Grass-roots synthesize", new DateTime(481, 5, 14, 22, 54, 46, 596, DateTimeKind.Unspecified).AddTicks(1616), "Massachusetts", 61, 11, 1 },
                    { 205, new DateTime(429, 7, 30, 19, 30, 25, 795, DateTimeKind.Unspecified).AddTicks(5568), "HDD Crossing explicit", new DateTime(2020, 7, 27, 2, 33, 20, 78, DateTimeKind.Unspecified).AddTicks(6000), "Road", 15, 11, 1 },
                    { 227, new DateTime(1328, 5, 4, 2, 33, 16, 859, DateTimeKind.Unspecified).AddTicks(864), "Park bricks-and-clicks Future", new DateTime(534, 3, 30, 15, 23, 46, 112, DateTimeKind.Unspecified).AddTicks(656), "Taiwan", 28, 19, 3 },
                    { 213, new DateTime(971, 6, 30, 10, 37, 54, 677, DateTimeKind.Unspecified).AddTicks(576), "productize unleash Russian Ruble", new DateTime(516, 8, 7, 23, 44, 8, 847, DateTimeKind.Unspecified).AddTicks(8064), "Vermont", 44, 19, 1 },
                    { 211, new DateTime(495, 3, 2, 14, 3, 16, 956, DateTimeKind.Unspecified).AddTicks(5264), "Health Reactive Connecticut", new DateTime(148, 12, 26, 15, 16, 52, 844, DateTimeKind.Unspecified).AddTicks(8808), "bluetooth", 47, 19, 1 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 202, new DateTime(1395, 2, 2, 22, 52, 59, 661, DateTimeKind.Unspecified).AddTicks(9088), "PNG Lead context-sensitive", new DateTime(1368, 9, 8, 23, 50, 33, 62, DateTimeKind.Unspecified).AddTicks(9520), "Island", 78, 19, 4 },
                    { 191, new DateTime(516, 6, 4, 11, 26, 46, 395, DateTimeKind.Unspecified).AddTicks(6944), "leverage Quality-focused Libyan Arab Jamahiriya", new DateTime(496, 11, 29, 22, 48, 5, 199, DateTimeKind.Unspecified).AddTicks(5984), "Lebanese Pound", 94, 19, 4 },
                    { 180, new DateTime(1044, 1, 13, 8, 3, 14, 208, DateTimeKind.Unspecified).AddTicks(5008), "Executive Clothing, Kids & Industrial Automotive, Shoes & Garden", new DateTime(1484, 6, 19, 6, 27, 16, 279, DateTimeKind.Unspecified).AddTicks(3168), "impactful", 13, 19, 3 },
                    { 162, new DateTime(1173, 12, 20, 11, 28, 21, 301, DateTimeKind.Unspecified).AddTicks(9024), "Money Market Account CFP Franc connect", new DateTime(1450, 2, 6, 22, 16, 21, 643, DateTimeKind.Unspecified).AddTicks(5280), "azure", 5, 19, 3 },
                    { 140, new DateTime(1994, 5, 15, 18, 42, 42, 180, DateTimeKind.Unspecified).AddTicks(6800), "cross-platform Sao Tome and Principe Ergonomic", new DateTime(961, 7, 12, 11, 50, 50, 83, DateTimeKind.Unspecified).AddTicks(7392), "reinvent", 62, 19, 3 },
                    { 135, new DateTime(830, 10, 12, 11, 11, 46, 779, DateTimeKind.Unspecified).AddTicks(9888), "black Steel open-source", new DateTime(188, 2, 21, 11, 41, 18, 200, DateTimeKind.Unspecified).AddTicks(4648), "Ergonomic Rubber Sausages", 77, 19, 4 },
                    { 95, new DateTime(1745, 2, 23, 6, 25, 31, 460, DateTimeKind.Unspecified).AddTicks(5904), "Developer Buckinghamshire Identity", new DateTime(981, 4, 8, 4, 21, 44, 743, DateTimeKind.Unspecified).AddTicks(9632), "payment", 56, 19, 2 },
                    { 85, new DateTime(1918, 1, 7, 17, 13, 26, 781, DateTimeKind.Unspecified).AddTicks(5888), "Berkshire Handcrafted eyeballs", new DateTime(1745, 8, 1, 9, 32, 5, 22, DateTimeKind.Unspecified).AddTicks(8880), "Awesome Concrete Towels", 96, 19, 3 },
                    { 36, new DateTime(1035, 12, 18, 3, 44, 22, 222, DateTimeKind.Unspecified).AddTicks(8496), "Handcrafted Rubber Table Mews port", new DateTime(573, 5, 6, 4, 14, 57, 702, DateTimeKind.Unspecified).AddTicks(6928), "Palestinian Territory", 16, 19, 2 },
                    { 4, new DateTime(1778, 9, 12, 11, 23, 38, 20, DateTimeKind.Unspecified).AddTicks(8592), "hack Grocery, Grocery & Music Licensed Granite Computer", new DateTime(339, 9, 3, 7, 30, 34, 523, DateTimeKind.Unspecified).AddTicks(1712), "digital", 79, 19, 2 },
                    { 217, new DateTime(1927, 12, 20, 17, 6, 52, 852, DateTimeKind.Unspecified).AddTicks(5136), "invoice Bedfordshire Internal", new DateTime(1768, 9, 6, 12, 0, 1, 77, DateTimeKind.Unspecified).AddTicks(1984), "Coves", 82, 11, 2 },
                    { 207, new DateTime(1310, 6, 14, 19, 55, 7, 655, DateTimeKind.Unspecified).AddTicks(5600), "homogeneous Sleek moderator", new DateTime(1503, 1, 5, 14, 30, 51, 46, DateTimeKind.Unspecified).AddTicks(8624), "Borders", 75, 11, 2 },
                    { 192, new DateTime(25, 10, 21, 8, 3, 4, 418, DateTimeKind.Unspecified).AddTicks(4494), "24/7 port North Dakota", new DateTime(1089, 2, 1, 11, 47, 16, 268, DateTimeKind.Unspecified).AddTicks(3792), "Administrator", 87, 11, 3 },
                    { 147, new DateTime(893, 11, 27, 13, 17, 29, 283, DateTimeKind.Unspecified).AddTicks(1568), "Villages Metal navigating", new DateTime(1200, 4, 20, 4, 35, 36, 357, DateTimeKind.Unspecified).AddTicks(1664), "Plains", 100, 17, 4 },
                    { 150, new DateTime(1316, 4, 1, 17, 15, 5, 37, DateTimeKind.Unspecified).AddTicks(9408), "Vanuatu communities solid state", new DateTime(98, 1, 25, 2, 55, 17, 231, DateTimeKind.Unspecified).AddTicks(3280), "Re-contextualized", 49, 17, 1 },
                    { 166, new DateTime(1552, 7, 26, 14, 57, 36, 329, DateTimeKind.Unspecified).AddTicks(9856), "Wyoming Spur Brazil", new DateTime(147, 6, 15, 21, 3, 21, 429, DateTimeKind.Unspecified).AddTicks(3632), "Configuration", 92, 17, 1 },
                    { 127, new DateTime(974, 6, 24, 14, 56, 57, 895, DateTimeKind.Unspecified).AddTicks(7008), "hard drive solid state Camp", new DateTime(789, 1, 1, 13, 6, 32, 143, DateTimeKind.Unspecified).AddTicks(7776), "Congo", 52, 16, 4 },
                    { 103, new DateTime(1986, 3, 20, 5, 22, 28, 326, DateTimeKind.Unspecified).AddTicks(6384), "Dale Direct virtual", new DateTime(88, 7, 19, 7, 35, 42, 267, DateTimeKind.Unspecified).AddTicks(248), "Checking Account", 45, 16, 3 },
                    { 94, new DateTime(561, 3, 27, 17, 48, 4, 747, DateTimeKind.Unspecified).AddTicks(1952), "black card Maryland", new DateTime(1167, 10, 16, 11, 18, 53, 944, DateTimeKind.Unspecified).AddTicks(5520), "indexing", 3, 16, 1 },
                    { 63, new DateTime(905, 7, 30, 6, 31, 58, 122, DateTimeKind.Unspecified).AddTicks(6512), "installation Dynamic indigo", new DateTime(118, 4, 4, 2, 23, 4, 249, DateTimeKind.Unspecified).AddTicks(8520), "Optional", 88, 16, 4 },
                    { 35, new DateTime(1293, 7, 12, 16, 42, 41, 753, DateTimeKind.Unspecified).AddTicks(9280), "orange Prairie override", new DateTime(1086, 2, 9, 0, 50, 34, 154, DateTimeKind.Unspecified).AddTicks(8240), "open system", 83, 16, 3 },
                    { 25, new DateTime(12, 9, 8, 3, 48, 31, 622, DateTimeKind.Unspecified).AddTicks(8574), "utilize solutions Singapore Dollar", new DateTime(1039, 2, 19, 23, 55, 40, 767, DateTimeKind.Unspecified).AddTicks(8416), "plug-and-play", 36, 16, 2 },
                    { 12, new DateTime(1145, 10, 2, 9, 0, 5, 122, DateTimeKind.Unspecified).AddTicks(3440), "PNG Key Drives", new DateTime(1239, 2, 17, 14, 41, 13, 501, DateTimeKind.Unspecified).AddTicks(9664), "time-frame", 92, 16, 2 },
                    { 288, new DateTime(1633, 2, 8, 9, 37, 18, 426, DateTimeKind.Unspecified).AddTicks(2928), "connecting schemas Knolls", new DateTime(648, 9, 22, 6, 51, 24, 514, DateTimeKind.Unspecified).AddTicks(6288), "Handmade Soft Pants", 74, 20, 4 },
                    { 275, new DateTime(665, 4, 10, 5, 4, 23, 523, DateTimeKind.Unspecified).AddTicks(8320), "Belarus Berkshire Investment Account", new DateTime(58, 8, 9, 8, 10, 50, 247, DateTimeKind.Unspecified).AddTicks(4292), "Nebraska", 88, 20, 2 },
                    { 261, new DateTime(971, 11, 14, 8, 56, 49, 728, DateTimeKind.Unspecified).AddTicks(9552), "open-source Bedfordshire connect", new DateTime(1448, 4, 4, 11, 3, 9, 182, DateTimeKind.Unspecified).AddTicks(1072), "Iraq", 77, 20, 3 },
                    { 241, new DateTime(1860, 1, 6, 18, 11, 27, 32, DateTimeKind.Unspecified).AddTicks(5968), "Borders Pike online", new DateTime(37, 12, 28, 14, 5, 4, 618, DateTimeKind.Unspecified).AddTicks(9346), "application", 78, 20, 1 },
                    { 228, new DateTime(766, 7, 23, 19, 40, 59, 194, DateTimeKind.Unspecified).AddTicks(3984), "Court capacitor bypass", new DateTime(560, 3, 16, 7, 9, 16, 641, DateTimeKind.Unspecified).AddTicks(288), "transform", 71, 20, 2 },
                    { 216, new DateTime(452, 11, 25, 1, 9, 46, 33, DateTimeKind.Unspecified).AddTicks(1392), "index Squares Illinois", new DateTime(420, 4, 9, 16, 20, 13, 334, DateTimeKind.Unspecified).AddTicks(3248), "optimal", 52, 20, 4 },
                    { 214, new DateTime(225, 9, 13, 21, 31, 5, 157, DateTimeKind.Unspecified).AddTicks(6760), "Central Intelligent Plastic Shirt Nebraska", new DateTime(1438, 3, 9, 6, 4, 47, 61, DateTimeKind.Unspecified).AddTicks(768), "Incredible Steel Pizza", 76, 20, 1 },
                    { 200, new DateTime(1179, 11, 23, 1, 43, 32, 977, DateTimeKind.Unspecified).AddTicks(7488), "Investment Account Bahamas Home, Kids & Automotive", new DateTime(1074, 2, 1, 18, 31, 20, 989, DateTimeKind.Unspecified).AddTicks(9600), "pixel", 50, 20, 2 },
                    { 128, new DateTime(219, 3, 31, 10, 1, 56, 463, DateTimeKind.Unspecified).AddTicks(944), "Handcrafted Steel Cheese Incredible Concrete Tuna Functionality", new DateTime(1411, 12, 28, 5, 31, 37, 939, DateTimeKind.Unspecified).AddTicks(6432), "interactive", 33, 16, 2 },
                    { 197, new DateTime(22, 8, 10, 13, 8, 41, 530, DateTimeKind.Unspecified).AddTicks(708), "Licensed Steel Keyboard deposit Officer", new DateTime(551, 11, 10, 15, 41, 1, 750, DateTimeKind.Unspecified).AddTicks(9744), "motivating", 29, 20, 2 },
                    { 157, new DateTime(863, 4, 18, 17, 17, 6, 682, DateTimeKind.Unspecified).AddTicks(7696), "Up-sized Isle 5th generation", new DateTime(98, 4, 12, 23, 48, 17, 332, DateTimeKind.Unspecified).AddTicks(3000), "Avon", 30, 16, 4 },
                    { 186, new DateTime(1008, 7, 17, 9, 8, 35, 918, DateTimeKind.Unspecified).AddTicks(6192), "Pula Gorgeous Soft Ball target", new DateTime(41, 2, 16, 15, 34, 53, 787, DateTimeKind.Unspecified).AddTicks(5720), "Plastic", 23, 16, 1 },
                    { 136, new DateTime(1127, 1, 12, 2, 12, 28, 812, DateTimeKind.Unspecified).AddTicks(4816), "Arizona deposit scale", new DateTime(313, 2, 7, 19, 9, 1, 785, DateTimeKind.Unspecified).AddTicks(1024), "Small Steel Pizza", 81, 9, 3 },
                    { 133, new DateTime(1076, 8, 6, 9, 12, 28, 651, DateTimeKind.Unspecified).AddTicks(3936), "Creative unleash invoice", new DateTime(911, 12, 23, 5, 32, 3, 474, DateTimeKind.Unspecified).AddTicks(4944), "Lead", 91, 9, 4 },
                    { 100, new DateTime(167, 12, 2, 4, 12, 32, 471, DateTimeKind.Unspecified).AddTicks(6800), "Rustic Cotton Chicken Global Cotton", new DateTime(515, 1, 26, 2, 21, 2, 667, DateTimeKind.Unspecified).AddTicks(7616), "Books, Music & Home", 17, 9, 2 },
                    { 92, new DateTime(1736, 7, 26, 9, 24, 39, 433, DateTimeKind.Unspecified).AddTicks(1536), "Incredible transmit calculating", new DateTime(1999, 3, 10, 4, 4, 4, 514, DateTimeKind.Unspecified).AddTicks(176), "Rapids", 73, 9, 4 },
                    { 71, new DateTime(1293, 2, 5, 20, 40, 39, 542, DateTimeKind.Unspecified).AddTicks(176), "Azerbaijanian Manat Alabama invoice", new DateTime(538, 8, 24, 5, 16, 19, 529, DateTimeKind.Unspecified).AddTicks(928), "Belarussian Ruble", 10, 9, 1 },
                    { 43, new DateTime(757, 6, 23, 21, 57, 43, 227, DateTimeKind.Unspecified).AddTicks(5792), "olive Checking Account Movies", new DateTime(379, 6, 14, 9, 41, 50, 169, DateTimeKind.Unspecified).AddTicks(2864), "Borders", 97, 9, 4 },
                    { 28, new DateTime(227, 4, 24, 7, 8, 51, 611, DateTimeKind.Unspecified).AddTicks(1656), "Handcrafted Granite Fish SSL Palladium", new DateTime(176, 11, 12, 22, 46, 28, 30, DateTimeKind.Unspecified).AddTicks(2448), "Cross-platform", 9, 9, 4 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 26, new DateTime(534, 7, 7, 11, 31, 22, 461, DateTimeKind.Unspecified).AddTicks(9632), "Route Delaware AGP", new DateTime(246, 10, 25, 11, 38, 35, 321, DateTimeKind.Unspecified).AddTicks(9168), "recontextualize", 85, 9, 2 },
                    { 292, new DateTime(1865, 2, 13, 6, 56, 28, 850, DateTimeKind.Unspecified).AddTicks(816), "Sleek Credit Card Account Garden & Computers", new DateTime(1631, 12, 4, 21, 2, 32, 425, DateTimeKind.Unspecified).AddTicks(9408), "Industrial, Clothing & Shoes", 42, 16, 1 },
                    { 290, new DateTime(1997, 8, 23, 14, 37, 59, 587, DateTimeKind.Unspecified).AddTicks(800), "Intelligent Wooden Computer turquoise Nevada", new DateTime(1419, 3, 1, 23, 46, 30, 851, DateTimeKind.Unspecified).AddTicks(4512), "Drives", 69, 16, 3 },
                    { 258, new DateTime(1490, 1, 7, 13, 58, 58, 998, DateTimeKind.Unspecified).AddTicks(5232), "Chile solution Ireland", new DateTime(1767, 3, 28, 11, 34, 55, 848, DateTimeKind.Unspecified).AddTicks(3216), "copying", 58, 16, 2 },
                    { 252, new DateTime(1364, 1, 13, 19, 40, 18, 781, DateTimeKind.Unspecified).AddTicks(6528), "Pa'anga attitude-oriented sensor", new DateTime(240, 4, 11, 15, 54, 31, 227, DateTimeKind.Unspecified).AddTicks(6288), "Avon", 63, 16, 2 },
                    { 249, new DateTime(1443, 4, 19, 15, 36, 0, 774, DateTimeKind.Unspecified).AddTicks(1776), "standardization Namibia value-added", new DateTime(867, 7, 22, 16, 49, 53, 126, DateTimeKind.Unspecified).AddTicks(4528), "View", 41, 16, 3 },
                    { 221, new DateTime(1850, 5, 17, 5, 29, 48, 773, DateTimeKind.Unspecified).AddTicks(4224), "Bolivar Fuerte Agent Solomon Islands Dollar", new DateTime(1086, 10, 10, 16, 39, 28, 943, DateTimeKind.Unspecified).AddTicks(4512), "Money Market Account", 15, 16, 2 },
                    { 208, new DateTime(1239, 6, 13, 17, 10, 27, 742, DateTimeKind.Unspecified).AddTicks(4080), "unleash dynamic Games, Clothing & Beauty", new DateTime(1487, 7, 27, 20, 31, 35, 750, DateTimeKind.Unspecified).AddTicks(7728), "optical", 24, 16, 3 },
                    { 160, new DateTime(2013, 8, 15, 16, 0, 26, 273, DateTimeKind.Unspecified).AddTicks(7488), "quantifying backing up lavender", new DateTime(1342, 8, 26, 13, 2, 30, 182, DateTimeKind.Unspecified).AddTicks(1776), "Western Sahara", 78, 16, 1 },
                    { 172, new DateTime(1401, 4, 3, 14, 41, 3, 601, DateTimeKind.Unspecified).AddTicks(1408), "Landing Operations Accountability", new DateTime(1905, 6, 30, 12, 3, 17, 229, DateTimeKind.Unspecified).AddTicks(4480), "blue", 44, 10, 1 },
                    { 194, new DateTime(148, 10, 30, 2, 21, 16, 516, DateTimeKind.Unspecified).AddTicks(7464), "multi-byte withdrawal Savings Account", new DateTime(38, 6, 15, 1, 45, 8, 330, DateTimeKind.Unspecified).AddTicks(3228), "Cuba", 74, 20, 4 },
                    { 163, new DateTime(1410, 1, 2, 19, 44, 33, 529, DateTimeKind.Unspecified).AddTicks(7808), "dynamic Executive viral", new DateTime(1823, 8, 24, 5, 24, 6, 878, DateTimeKind.Unspecified).AddTicks(5552), "Gorgeous Rubber Gloves", 84, 20, 3 },
                    { 230, new DateTime(722, 10, 13, 6, 38, 20, 592, DateTimeKind.Unspecified).AddTicks(1712), "open-source Park Engineer", new DateTime(1244, 3, 10, 19, 58, 42, 197, DateTimeKind.Unspecified).AddTicks(7680), "Web", 47, 8, 4 },
                    { 226, new DateTime(679, 11, 11, 7, 2, 22, 92, DateTimeKind.Unspecified).AddTicks(6416), "Marshall Islands explicit empower", new DateTime(443, 1, 11, 22, 29, 27, 634, DateTimeKind.Unspecified).AddTicks(2496), "Cotton", 17, 8, 2 },
                    { 179, new DateTime(1110, 7, 13, 19, 36, 59, 809, DateTimeKind.Unspecified).AddTicks(448), "groupware e-tailers Key", new DateTime(1835, 2, 14, 20, 36, 55, 521, DateTimeKind.Unspecified).AddTicks(8128), "Money Market Account", 80, 8, 1 },
                    { 175, new DateTime(86, 2, 19, 19, 7, 32, 902, DateTimeKind.Unspecified).AddTicks(3192), "Intelligent Steel Shirt transmitting front-end", new DateTime(170, 8, 10, 15, 52, 40, 212, DateTimeKind.Unspecified).AddTicks(9136), "functionalities", 60, 8, 1 },
                    { 148, new DateTime(2008, 2, 2, 13, 15, 12, 303, DateTimeKind.Unspecified).AddTicks(9312), "array driver Row", new DateTime(986, 10, 10, 18, 53, 42, 153, DateTimeKind.Unspecified).AddTicks(8448), "Light", 88, 8, 2 },
                    { 86, new DateTime(366, 6, 18, 10, 55, 18, 785, DateTimeKind.Unspecified).AddTicks(2176), "Arizona withdrawal capacity", new DateTime(711, 4, 5, 2, 28, 48, 909, DateTimeKind.Unspecified).AddTicks(3424), "generating", 59, 8, 1 },
                    { 82, new DateTime(382, 12, 6, 3, 21, 52, 131, DateTimeKind.Unspecified).AddTicks(6768), "back-end Drives Metal", new DateTime(1717, 9, 1, 9, 12, 47, 214, DateTimeKind.Unspecified).AddTicks(816), "Berkshire", 95, 8, 3 },
                    { 31, new DateTime(1014, 4, 19, 0, 59, 18, 269, DateTimeKind.Unspecified).AddTicks(2944), "Honduras Suriname Generic Rubber Sausages", new DateTime(1561, 10, 26, 5, 36, 41, 402, DateTimeKind.Unspecified).AddTicks(4272), "user-facing", 52, 8, 1 },
                    { 27, new DateTime(1491, 6, 26, 3, 27, 6, 445, DateTimeKind.Unspecified).AddTicks(9728), "mobile Interactions Oman", new DateTime(1557, 1, 2, 21, 8, 6, 377, DateTimeKind.Unspecified).AddTicks(1728), "withdrawal", 50, 8, 4 },
                    { 23, new DateTime(337, 4, 25, 10, 34, 17, 975, DateTimeKind.Unspecified).AddTicks(8816), "deliverables Overpass virtual", new DateTime(1353, 3, 29, 7, 40, 57, 19, DateTimeKind.Unspecified).AddTicks(4384), "Oklahoma", 68, 8, 1 },
                    { 17, new DateTime(213, 11, 26, 3, 59, 1, 505, DateTimeKind.Unspecified).AddTicks(9720), "Alley Buckinghamshire supply-chains", new DateTime(859, 2, 16, 17, 27, 30, 199, DateTimeKind.Unspecified).AddTicks(2656), "Convertible Marks", 15, 8, 2 },
                    { 210, new DateTime(1601, 11, 12, 19, 36, 36, 682, DateTimeKind.Unspecified).AddTicks(6192), "Outdoors, Games & Industrial Awesome Concrete Car Slovakia (Slovak Republic)", new DateTime(1514, 5, 10, 16, 4, 47, 245, DateTimeKind.Unspecified).AddTicks(7104), "brand", 51, 17, 2 },
                    { 203, new DateTime(1248, 3, 13, 14, 47, 29, 61, DateTimeKind.Unspecified).AddTicks(4160), "South Carolina gold mobile", new DateTime(754, 11, 16, 6, 57, 17, 569, DateTimeKind.Unspecified).AddTicks(9376), "dot-com", 41, 17, 2 },
                    { 178, new DateTime(772, 2, 7, 5, 39, 4, 150, DateTimeKind.Unspecified).AddTicks(5296), "Mission Saudi Riyal ROI", new DateTime(1501, 6, 24, 18, 0, 12, 973, DateTimeKind.Unspecified).AddTicks(9728), "Run", 65, 17, 2 },
                    { 168, new DateTime(764, 8, 10, 23, 24, 8, 660, DateTimeKind.Unspecified).AddTicks(7760), "Rustic Frozen Chicken override Triple-buffered", new DateTime(1634, 3, 13, 7, 24, 28, 917, DateTimeKind.Unspecified).AddTicks(8320), "AGP", 47, 17, 3 },
                    { 247, new DateTime(309, 5, 5, 4, 30, 21, 8, DateTimeKind.Unspecified).AddTicks(5392), "Beauty Buckinghamshire Metal", new DateTime(165, 6, 13, 4, 15, 43, 57, DateTimeKind.Unspecified).AddTicks(2744), "application", 23, 8, 3 },
                    { 193, new DateTime(1816, 1, 3, 17, 10, 0, 727, DateTimeKind.Unspecified).AddTicks(2592), "California Ergonomic Rubber Ball magenta", new DateTime(1199, 4, 16, 17, 35, 53, 844, DateTimeKind.Unspecified).AddTicks(2320), "eco-centric", 76, 20, 4 },
                    { 248, new DateTime(1545, 5, 12, 12, 34, 38, 423, DateTimeKind.Unspecified).AddTicks(2080), "Saint Helena Forward magenta", new DateTime(1970, 2, 9, 9, 20, 26, 80, DateTimeKind.Unspecified).AddTicks(6864), "Walks", 63, 8, 4 },
                    { 2, new DateTime(474, 11, 5, 17, 2, 50, 182, DateTimeKind.Unspecified).AddTicks(9680), "port bypassing salmon", new DateTime(1363, 12, 6, 16, 18, 13, 104, DateTimeKind.Unspecified).AddTicks(4880), "Electronics & Clothing", 85, 3, 1 },
                    { 138, new DateTime(1997, 2, 16, 21, 59, 37, 935, DateTimeKind.Unspecified).AddTicks(9568), "demand-driven Brand array", new DateTime(717, 5, 1, 0, 31, 30, 106, DateTimeKind.Unspecified).AddTicks(272), "Maryland", 58, 20, 3 },
                    { 116, new DateTime(1196, 9, 27, 4, 4, 53, 94, DateTimeKind.Unspecified).AddTicks(8304), "Malagasy Ariary Practical Metal Chips reboot", new DateTime(1427, 2, 4, 10, 13, 44, 637, DateTimeKind.Unspecified).AddTicks(4160), "Missouri", 92, 20, 3 },
                    { 111, new DateTime(1068, 3, 26, 22, 35, 18, 640, DateTimeKind.Unspecified).AddTicks(6800), "Borders navigating open-source", new DateTime(226, 3, 11, 17, 8, 55, 432, DateTimeKind.Unspecified).AddTicks(2152), "Concrete", 97, 20, 3 },
                    { 108, new DateTime(657, 10, 4, 8, 54, 37, 904, DateTimeKind.Unspecified).AddTicks(4944), "parse Michigan multi-byte", new DateTime(1500, 6, 28, 15, 17, 16, 88, DateTimeKind.Unspecified).AddTicks(4624), "payment", 16, 20, 1 },
                    { 61, new DateTime(990, 3, 3, 5, 25, 18, 896, DateTimeKind.Unspecified).AddTicks(5840), "PCI Personal Loan Account Frozen", new DateTime(1649, 8, 30, 18, 16, 40, 778, DateTimeKind.Unspecified).AddTicks(4592), "bypass", 60, 20, 4 },
                    { 38, new DateTime(1157, 5, 17, 10, 38, 27, 804, DateTimeKind.Unspecified).AddTicks(8464), "bandwidth Intelligent Rubber Sausages hacking", new DateTime(856, 4, 4, 17, 46, 25, 326, DateTimeKind.Unspecified).AddTicks(592), "Outdoors, Shoes & Grocery", 84, 20, 2 },
                    { 1, new DateTime(753, 7, 6, 23, 29, 44, 766, DateTimeKind.Unspecified).AddTicks(624), "violet payment Frozen", new DateTime(1001, 9, 29, 16, 27, 34, 264, DateTimeKind.Unspecified).AddTicks(4816), "Handmade", 53, 20, 1 },
                    { 224, new DateTime(1418, 7, 24, 2, 52, 9, 774, DateTimeKind.Unspecified).AddTicks(4016), "Manager Rustic Concrete Towels synthesizing", new DateTime(882, 11, 18, 7, 54, 37, 193, DateTimeKind.Unspecified).AddTicks(7776), "Practical", 99, 3, 2 },
                    { 212, new DateTime(939, 7, 3, 5, 56, 21, 147, DateTimeKind.Unspecified).AddTicks(2912), "compress Slovakia (Slovak Republic) pixel", new DateTime(1460, 5, 2, 11, 1, 52, 289, DateTimeKind.Unspecified).AddTicks(6336), "violet", 85, 3, 3 },
                    { 206, new DateTime(1056, 8, 13, 12, 10, 5, 592, DateTimeKind.Unspecified).AddTicks(9040), "firewall Tasty EXE", new DateTime(758, 12, 23, 16, 59, 48, 746, DateTimeKind.Unspecified).AddTicks(6416), "bypass", 16, 3, 2 },
                    { 198, new DateTime(441, 4, 16, 18, 21, 35, 153, DateTimeKind.Unspecified).AddTicks(5904), "Oregon unleash IB", new DateTime(1493, 11, 10, 1, 57, 18, 783, DateTimeKind.Unspecified).AddTicks(9120), "Mali", 57, 3, 2 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 170, new DateTime(1286, 8, 28, 6, 55, 27, 43, DateTimeKind.Unspecified).AddTicks(1376), "Accountability Ergonomic Frozen Ball Small Soft Shirt", new DateTime(916, 10, 6, 13, 3, 25, 693, DateTimeKind.Unspecified).AddTicks(9216), "neural-net", 34, 3, 2 },
                    { 167, new DateTime(1849, 3, 13, 1, 37, 10, 99, DateTimeKind.Unspecified).AddTicks(4000), "Officer International Nepal", new DateTime(1367, 11, 13, 14, 10, 26, 320, DateTimeKind.Unspecified).AddTicks(7312), "withdrawal", 86, 3, 2 },
                    { 106, new DateTime(1152, 10, 6, 14, 50, 43, 248, DateTimeKind.Unspecified).AddTicks(4240), "hack Industrial, Clothing & Clothing Direct", new DateTime(831, 5, 31, 8, 16, 38, 447, DateTimeKind.Unspecified).AddTicks(1056), "Common", 66, 3, 1 },
                    { 74, new DateTime(1896, 5, 25, 0, 49, 40, 342, DateTimeKind.Unspecified).AddTicks(5232), "calculate migration Generic", new DateTime(171, 8, 1, 19, 29, 1, 149, DateTimeKind.Unspecified).AddTicks(1208), "Developer", 90, 3, 3 },
                    { 259, new DateTime(1248, 1, 6, 9, 49, 55, 178, DateTimeKind.Unspecified).AddTicks(2480), "grey silver Centers", new DateTime(1213, 5, 2, 20, 10, 29, 970, DateTimeKind.Unspecified).AddTicks(9328), "Metal", 80, 8, 3 },
                    { 190, new DateTime(1549, 6, 23, 21, 52, 40, 132, DateTimeKind.Unspecified).AddTicks(9424), "Movies, Jewelery & Electronics extensible Cliff", new DateTime(1358, 10, 27, 1, 40, 30, 385, DateTimeKind.Unspecified).AddTicks(5888), "Falkland Islands Pound", 82, 10, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_AuthorId",
                table: "Projects",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_TeamId",
                table: "Projects",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_PerformerId",
                table: "Tasks",
                column: "PerformerId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_TeamId",
                table: "Users",
                column: "TeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
