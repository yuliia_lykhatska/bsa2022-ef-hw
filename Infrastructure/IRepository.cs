﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastucture
{
    public interface IRepository<T> where T : class
    {
        public void Create(T entity);
        public T Read(int id);
        public List<T> Read();
        public void Update(T entity);
        public void Delete(int id);

    }
}
