﻿using Domain.Entities;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastucture
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly LinqDbContext context;

        public TeamRepository(LinqDbContext _context)
        {
            context = _context;
        }

        public void Create(Team entity)
        {
            context.Teams.Add(entity);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var entity = Read(id);
            if (entity != null)
            {
                context.Teams.Attach(entity);
                context.Teams.Remove(entity);
                context.SaveChanges();
            }
        }

        public Team Read(int id)
        {
            return context.Teams.FirstOrDefault(Team => Team.Id == id);
        }

        public List<Team> Read()
        {
            return context.Teams.ToList();
        }

        public void Update(Team entity)
        {
            var entityToUpdate = Read(entity.Id);
            foreach (var property in typeof(Team).GetProperties())
            {
                var value = property.GetValue(entity);
                if (value != null)
                {
                    property.SetValue(entityToUpdate, value);
                }
            }
            context.SaveChanges();
        }
    }
}
