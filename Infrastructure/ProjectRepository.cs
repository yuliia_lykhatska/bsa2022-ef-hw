﻿using Domain.Entities;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastucture
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly LinqDbContext context;

        public ProjectRepository(LinqDbContext _context)
        {
            context = _context;
        }

        public void Create(Project entity)
        {
            context.Projects.Add(entity);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var entity = Read(id);
            if (entity != null) {
                context.Projects.Attach(entity);
                context.Projects.Remove(entity);
                context.SaveChanges();
            }
        }

        public Project Read(int id)
        {
            return context.Projects.FirstOrDefault(project => project.Id == id);
        }

        public List<Project> Read()
        {
            return context.Projects.ToList();
        }

        public void Update(Project entity)
        {
            var entityToUpdate = Read(entity.Id);
            foreach (var property in typeof(User).GetProperties())
            {
                var value = property.GetValue(entity);
                if (value != null)
                {
                    property.SetValue(entityToUpdate, value);
                }
            }
            context.SaveChanges();
        }
    }
}
