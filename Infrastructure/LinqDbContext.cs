﻿using Bogus;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    public class LinqDbContext : DbContext
    {
        public LinqDbContext(DbContextOptions<LinqDbContext> options) 
            :base(options)
        {    }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {


            builder.Entity<Task>()
                .HasOne(task => task.Project)
                .WithMany(project => project.Tasks)
                .HasForeignKey(task => task.ProjectId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<Task>()
                .HasOne(task => task.Performer)
                .WithMany(user => user.Tasks)
                .HasForeignKey(task => task.PerformerId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<Team>()
                .HasMany(team => team.Members)
                .WithOne(user => user.Team)
                .HasForeignKey(user => user.TeamId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<Project>()
                .HasOne(project => project.Author)
                .WithMany(user => user.Projects)
                .HasForeignKey(project => project.AuthorId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Project>()
                .HasOne(project => project.Team)
                .WithMany(team => team.Projects)
                .HasForeignKey(project => project.TeamId)
                .OnDelete(DeleteBehavior.Restrict);

            var teams = new List<Team>();
/*            {
                new Team { Id = 1, Name = "Name1", CreatedAt = DateTime.Now },
                new Team { Id = 2, Name = "Name2", CreatedAt = DateTime.Now },
                new Team { Id = 3, Name = "Name3", CreatedAt = DateTime.Now }
            };*/


            var teamFaker = new Faker<Team>()
                .RuleFor(t => t.Id, 0)
                .RuleFor(t => t.Name, f => f.Random.Word())
                .RuleFor(t => t.CreatedAt, f => f.Date.Between(new System.DateTime(2000), System.DateTime.Now));
            
            for (int i = 0; i < 20; i++)
            {
                teams.Add(teamFaker.Generate());
                teams[i].Id = i + 1;
            }
            builder.Entity<Team>().HasData(teams);

            var users = new List<User>();
            var userFaker = new Faker<User>()
                .RuleFor(u => u.Id, 0)
                .RuleFor(u => u.TeamId, f => f.Random.Int(1, 20))
                .RuleFor(u => u.FirstName, f => f.Random.Word())
                .RuleFor(u => u.LastName, f => f.Random.Word())
                .RuleFor(u => u.Email, f => f.Random.Word())
                .RuleFor(u => u.RegisteredAt, f => f.Date.Between(new System.DateTime(2000), System.DateTime.Now))
                .RuleFor(u => u.BirthDay, f => f.Date.Between(new System.DateTime(1940), new System.DateTime(2005)));

            for (int i = 0; i < 100; i++)
            {
                users.Add(userFaker.Generate());
                users[i].Id = i + 1;
            }
            builder.Entity<User>().HasData(users);

            var projects = new List<Project>();
            var projectFaker = new Faker<Project>()
                .RuleFor(p => p.Id, 0)
                .RuleFor(p => p.Name, f => f.Random.Word())
                .RuleFor(p => p.Description, f => f.Random.Words(3))
                .RuleFor(p => p.AuthorId, f => f.Random.Int(1, 20))
                .RuleFor(p => p.TeamId, f => f.Random.Int(1, 20))
                .RuleFor(p => p.CreatedAt, f => f.Date.Between(new System.DateTime(2000), System.DateTime.Now))
                .RuleFor(p => p.Deadline, f => f.Date.Between(new System.DateTime(2000), System.DateTime.Now));
            for (int i = 0; i < 20; i++)
            {
                projects.Add(projectFaker.Generate());
                projects[i].Id = i + 1;
            }
            builder.Entity<Project>().HasData(projects);

            var tasks = new List<Task>();
            var taskFaker = new Faker<Task>()
                .RuleFor(t => t.Id, 0)
                .RuleFor(t => t.ProjectId, f => f.Random.Int(1, 20))
                .RuleFor(t => t.PerformerId, f => f.Random.Int(1, 100))
                .RuleFor(t => t.Name, f => f.Random.Word())
                .RuleFor(t => t.Description, f => f.Random.Words(3))
                .RuleFor(t => t.State, f => f.Random.Int(1, 4))
                .RuleFor(t => t.CreatedAt, f => f.Date.Between(new System.DateTime(2000), System.DateTime.Now))
                .RuleFor(t => t.FinishedAt, f => f.Date.Between(new System.DateTime(2000), System.DateTime.Now));

            for (int i = 0; i < 300; i++)
            {
                tasks.Add(taskFaker.Generate());
                tasks[i].Id = i + 1;
            }
            builder.Entity<Task>().HasData(tasks);

            base.OnModelCreating(builder);
        }
    }
}
