﻿using System;

namespace bsa2022_linq_hw
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new DataService();
            service.GetDataReady();
            var check1 = service.CountTasksByEachProjectForUser(0);
            var check2 = service.GetTasksWithNameLongerThan45ForUser(0);
            var check3 = service.GetTasksFinishedIn2021ForUser(0);
            var check4 = service.GetTeamsWithMembersOlderThan10();
            var check5 = service.GetUsersWithTasks();
            Console.WriteLine("");
        }
    }
}
