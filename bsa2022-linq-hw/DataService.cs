﻿using bsa2022_linq_hw.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace bsa2022_linq_hw
{
    public class DataService
    {
        public Data Data { get; set; }
        private static readonly HttpClient client = new HttpClient();
        private string route = "https://localhost:44352/api";

        public DataService()
        {
            Data = Data.GetInstance();
        }

        public void GetDataReady()
        {
            LoadData().Wait();

            BuildProjectTaskConnections();
            BuildTaskPerformerConnections();
            BuildProjectAuthorConnections();
            BuildProjectTeamConnections();
            BuildTeamUsersConnections();
        }

        public async Task<List<Project>> LoadData()
        {
            string usersJson = await LoadCollection("users");
            string teamsJson = await LoadCollection("teams");
            string tasksJson = await LoadCollection("tasks");
            string projectsJson = await LoadCollection("projects");

            Data.Users = JsonConvert.DeserializeObject<List<User>>(usersJson, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            Data.Teams = JsonConvert.DeserializeObject<List<Team>>(teamsJson, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            Data.Tasks = JsonConvert.DeserializeObject<List<Entities.Task>>(tasksJson, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            Data.Projects = JsonConvert.DeserializeObject<List<Project>>(projectsJson, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

            return Data.Projects;
        }

        public async Task<string> LoadCollection(string collectionName)
        {
            HttpResponseMessage response = await client.GetAsync($"{route}/{collectionName}");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            return responseBody;
        }

        public void BuildTaskPerformerConnections()
        {
            var taskPerformersJoin = Data.Tasks.Join(
                Data.Users,
                task => task.PerformerId,
                user => user.Id,
                (task, user) => new
                {
                    task,
                    user
                });
            foreach (var item in taskPerformersJoin)
            {
                item.task.Performer = item.user;
            }
        }

        public void BuildProjectTaskConnections()
        {
            var tasksJoin = Data.Projects.Join(
              Data.Tasks,
              project => project.Id,
              task => task.ProjectId,
              (project, task) => new
              {
                  project,
                  task
              });
            foreach (var item in tasksJoin)
            {
                item.project.Tasks ??= new List<Entities.Task>();
                item.project.Tasks.Add(item.task);
            }
        }

        public void BuildTeamUsersConnections()
        {
            var membersJoin = Data.Teams.Join(
              Data.Users,
              team => team.Id,
              user => user.TeamId,
              (team, user) => new
              {
                  team,
                  user
              });
            foreach (var item in membersJoin)
            {
                item.team.Members ??= new List<User>();
                item.team.Members.Add(item.user);
            }
        }

        public void BuildProjectAuthorConnections()
        {
            var authorsJoin = Data.Projects.Join(
                Data.Users,
                project => project.AuthorId,
                user => user.Id,
                (project, user) => new
                {
                    project,
                    user
                });
            foreach (var item in authorsJoin)
            {
                item.project.Author = item.user;
            }
        }

        public void BuildProjectTeamConnections()
        {
            var teamsJoin = Data.Projects.Join(
                Data.Teams,
                project => project.TeamId,
                team => team.Id,
                (project, team) => new
                {
                    project,
                    team
                });
            foreach (var item in teamsJoin)
            {
                item.project.Team = item.team;
            }
        }

        public Dictionary<Project, int> CountTasksByEachProjectForUser(int userId)
        {
            var result = Data.Tasks
                .Where(task => task.PerformerId == userId)
                .GroupBy(task => task.ProjectId)
                .ToDictionary
                (
                    group => Data.Projects.FirstOrDefault(project => project.Id == group.Key),
                    group => group.Count()
                );
            return result;
        }

        public List<Entities.Task> GetTasksWithNameLongerThan45ForUser(int userId)
        {
            return Data.Tasks.Where(task => task.PerformerId == userId && task.Name.Length > 45).ToList();
        }

        public struct TaskInfo
        {
            public int id;
            public string name;
        }

        public List<TaskInfo> GetTasksFinishedIn2021ForUser(int userId)

        {
            var result = Data.Tasks
                .Where(task => task.FinishedAt.Year == 2021 && task.PerformerId == userId)
                .Select(project => new TaskInfo
                { 
                    id = project.Id, 
                    name = project.Name 
                });
            return result.ToList();
        }
        public struct TeamInfo
        {
            public int id;
            public string name;
            public List<User> members;
        }
        public List<TeamInfo> GetTeamsWithMembersOlderThan10()
        {
            var result = Data.Teams
                .Where(team => team.Members.All(member => DateTime.Now.Year - member.BirthDay.Year >= 10))
                .Select(team => new TeamInfo
                {
                    id = team.Id,
                    name = team.Name,
                    members = team.Members.OrderByDescending(member => member.RegisteredAt).ToList()
                });
            return result.ToList();
        }
        public List<(User, List<Entities.Task>)> GetUsersWithTasks()
        {
            var result = new List<(User, List<Entities.Task>)>();
            var taskPerformersJoin = Data.Tasks.Join(
                Data.Users,
                task => task.PerformerId,
                user => user.Id,
                (task, user) => new
                {
                    task,
                    user
                });
            foreach (var item in taskPerformersJoin)
            {
                item.user.Tasks ??= new List<Entities.Task>();
                item.user.Tasks.Add(item.task);
            }
           foreach(User user in taskPerformersJoin.Select(item => item.user))
            {
                result.Add((user, user.Tasks.OrderByDescending(task => task.Name).ToList()));
            }
            result = result.OrderBy(item => item.Item1.FirstName).ToList();
            return result;
        }

        public struct UserInfo
        {
            public User user;
            public int numberOfTasksOnTheLastProject;
            public int numberOfUnfinishedOrCancelledTasks;
            public Entities.Task longestTask;
        }


    }
}
