﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bsa2022_linq_hw.Entities
{
    public class User
    {
        public int Id { get; set; }
        public int TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
