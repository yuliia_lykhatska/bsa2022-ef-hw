﻿using bsa2022_linq_hw.Entities;
using System.Collections.Generic;


namespace bsa2022_linq_hw
{
    public class Data
    {
        private static Data instance;
        public List<Project> Projects { get; set; }
        public List<Task> Tasks { get; set; }
        public List<User> Users { get; set; }
        public List<Team> Teams { get; set; }
        private Data()
        {
            Projects = new List<Project>();
        }
        public static Data GetInstance()
        {
            if(instance == null)
            {
                instance = new Data();
            }
            return instance;
        }
    }
}
