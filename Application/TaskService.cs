﻿using Application.DTO;
using AutoMapper;
using Domain.Entities;
using Infrastucture;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application
{
    public class TaskService
    {
        private readonly IMapper mapper;
        private readonly IRepository<Task> repository;
     
        public TaskService(IRepository<Task> _repository, IMapper _mapper)
        {
            repository = _repository;
            mapper = _mapper;
        }
        public void Create(TaskDTO Task)
        {
            if (repository.Read(Task.Id) != null)
            {
                throw new InvalidOperationException("Task with this id already exists.");
            }
            repository.Create(mapper.Map<Task>(Task));
        }

        public TaskDTO Read(int id)
        {
            var entity = repository.Read(id);
            if (entity == null) { throw new KeyNotFoundException(); }
            return mapper.Map<TaskDTO>(entity);
        }

        public List<TaskDTO> Read()
        {
            return mapper.Map<List<TaskDTO>>(repository.Read());
        }

        public void Update(TaskDTO Task)
        {
            var entity = repository.Read(Task.Id);
            if (entity == null) { throw new KeyNotFoundException(); }
            repository.Update(mapper.Map<Task>(Task));
        }

        public void Delete(int id)
        {
            var entity = repository.Read(id);
            if (entity == null) { throw new KeyNotFoundException(); }
            repository.Delete(id);
        }
    }
}
