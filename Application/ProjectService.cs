﻿using Application.DTO;
using AutoMapper;
using Domain.Entities;
using Infrastucture;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application
{
    public class ProjectService
    {
        private readonly IRepository<Project> repository;
        private readonly IMapper mapper;
        public ProjectService(IRepository<Project> _repository, IMapper _mapper)
        {
            repository = _repository;
            mapper = _mapper;
        }
        public void Create(ProjectDTO project)
        {
            if (repository.Read(project.Id) != null) 
            {
                throw new InvalidOperationException("Project with this id already exists.");
            }
            repository.Create(mapper.Map<Project>(project));
        }

        public ProjectDTO Read(int id)
        {
            var entity = repository.Read(id);
            if( entity == null ) { throw new KeyNotFoundException(); }
            return mapper.Map<ProjectDTO>(entity);
        }

        public List<ProjectDTO> Read()
        {
            return mapper.Map<List<ProjectDTO>>(repository.Read());
        }

        public void Update(ProjectDTO project)
        {
            var entity = repository.Read(project.Id);
            if (entity == null) { throw new KeyNotFoundException(); }
            repository.Update(mapper.Map<Project>(project));
        }

        public void Delete(int id)
        {
            var entity = repository.Read(id);
            if (entity == null) { throw new KeyNotFoundException(); }
            repository.Delete(id);
        }


    }
}
