﻿using Application.DTO;
using AutoMapper;
using Domain.Entities;
using Infrastucture;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application
{
    public class UserService
    {
        private readonly IMapper mapper;
        private readonly IRepository<User> repository;

        public UserService(IRepository<User> _repository, IMapper _mapper)
        {
            repository = _repository;
            mapper = _mapper;
        }
        public void Create(UserDTO User)
        {
            if (repository.Read(User.Id) != null)
            {
                throw new InvalidOperationException("User with this id already exists.");
            }
            User entity = new User();
            mapper.Map<UserDTO, User>(User, entity);
            repository.Create(entity);
        }

        public UserDTO Read(int id)
        {
            var entity = repository.Read(id);
            if (entity == null) { throw new KeyNotFoundException(); }
            return mapper.Map<UserDTO>(entity);
        }

        public List<UserDTO> Read()
        {
            return mapper.Map<List<UserDTO>>(repository.Read());
        }

        public void Update(UserDTO User, int id)
        {
            if (User.Id != id) { throw new InvalidOperationException("Can not change object`s idS"); }
            var entity = repository.Read(id);
            if (entity == null) { throw new KeyNotFoundException(); }
            repository.Update(mapper.Map<User>(User));
        }

        public void Delete(int id)
        {
            var entity = repository.Read(id);
            if (entity == null) { throw new KeyNotFoundException(); }
            repository.Delete(id);
        }
    }
}
