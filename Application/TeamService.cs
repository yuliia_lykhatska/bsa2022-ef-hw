﻿using Application.DTO;
using AutoMapper;
using Domain.Entities;
using Infrastucture;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application
{
    public class TeamService
    {
        private readonly IMapper mapper;
        private readonly IRepository<Team> repository;

        public TeamService(IRepository<Team> _repository, IMapper _mapper)
        {
            repository = _repository;
            mapper = _mapper;
        }
        public void Create(TeamDTO Team)
        {
            if (repository.Read(Team.Id) != null)
            {
                throw new InvalidOperationException("Team with this id already exists.");
            }
            repository.Create(mapper.Map<Team>(Team));
        }

        public object Read(int id)
        {
            var entity = repository.Read(id);
            if (entity == null) { throw new KeyNotFoundException(); }
            return entity;
        }

        public List<TeamDTO> Read()
        {
            return mapper.Map<List<TeamDTO>>(repository.Read());
        }

        public void Update(TeamDTO Team)
        {
            var entity = repository.Read(Team.Id);
            if (entity == null) { throw new KeyNotFoundException(); }
            repository.Update(mapper.Map<Team>(Team));
        }

        public void Delete(int id)
        {
            var entity = repository.Read(id);
            if (entity == null) { throw new KeyNotFoundException(); }
            repository.Delete(id);
        }
    }
}