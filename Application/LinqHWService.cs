﻿using Application.DTO;
using AutoMapper;
using Domain.Entities;
using Infrastucture;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application
{
    public class LinqHWService
    {
        private readonly IRepository<User> users;
        private readonly IRepository<Task> tasks;
        private readonly IRepository<Team> teams;
        private readonly IRepository<Project> projects;
        private readonly IMapper mapper;
        public LinqHWService(IRepository<User> _users, IRepository<Task> _tasks, IRepository<Team> _teams, IRepository<Project> _projects, IMapper _mapper)
        {
            users = _users;
            tasks = _tasks;
            teams = _teams;
            projects = _projects;
            mapper = _mapper;

            BuildProjectTaskConnections();
            BuildTaskPerformerConnections();
            BuildProjectAuthorConnections();
            BuildProjectTeamConnections();
            BuildTeamUsersConnections();
        }

        public void BuildTaskPerformerConnections()
        {
            var taskPerformersJoin = tasks.Read().Join(
                users.Read(),
                task => task.PerformerId,
                user => user.Id,
                (task, user) => new
                {
                    task,
                    user
                });
            foreach (var item in taskPerformersJoin)
            {
                item.task.Performer = item.user;
            }
        }

        public void BuildProjectTaskConnections()
        {
            var tasksJoin = projects.Read().Join(
              tasks.Read(),
              project => project.Id,
              task => task.ProjectId,
              (project, task) => new
              {
                  project,
                  task
              });
            foreach (var item in tasksJoin)
            {
                item.project.Tasks ??= new List<Task>();
                item.project.Tasks.Add(item.task);
            }
        }

        public void BuildTeamUsersConnections()
        {
            var membersJoin = teams.Read().Join(
              users.Read(),
              team => team.Id,
              user => user.TeamId,
              (team, user) => new
              {
                  team,
                  user
              });
            foreach (var item in membersJoin)
            {
                item.team.Members ??= new List<User>();
                item.team.Members.Add(item.user);
            }
        }

        public void BuildProjectAuthorConnections()
        {
            var authorsJoin = projects.Read().Join(
                users.Read(),
                project => project.AuthorId,
                user => user.Id,
                (project, user) => new
                {
                    project,
                    user
                });
            foreach (var item in authorsJoin)
            {
                item.project.Author = item.user;
            }
        }

        public void BuildProjectTeamConnections()
        {
            var teamsJoin = projects.Read().Join(
                teams.Read(),
                project => project.TeamId,
                team => team.Id,
                (project, team) => new
                {
                    project,
                    team
                }) ;
            foreach (var item in teamsJoin)
            {
                item.project.Team = item.team;
            }
        }

        public Dictionary<ProjectDTO, int> CountTasksByEachProjectForUser(int userId)
        {
            var result = tasks.Read()
                .Where(task => task.PerformerId == userId)
                .GroupBy(task => task.ProjectId)
                .ToDictionary
                (
                    group => mapper.Map<ProjectDTO>(projects.Read().FirstOrDefault(project => project.Id == group.Key)),
                    group => group.Count()
                );
            return result;
        }

        public List<TaskDTO> GetTasksWithNameLongerThan45ForUser(int userId)
        {
            var entities =  tasks.Read().Where(task => task.PerformerId == userId && task.Name.Length > 45).ToList();
            return mapper.Map<List<TaskDTO>>(entities);
        }

        public struct TaskInfo
        {
            public int id;
            public string name;
        }

        public List<TaskInfo> GetTasksFinishedIn2021ForUser(int userId)
        {
            var result = tasks.Read()
                .Where(task => task.FinishedAt.Year == 2021 && task.PerformerId == userId)
                .Select(project => new TaskInfo
                {
                    id = project.Id,
                    name = project.Name
                });
            return result.ToList();
        }

        public struct TeamInfo
        {
            public int id;
            public string name;
            public List<User> members;
        }

        public List<TeamInfo> GetTeamsWithMembersOlderThan10()
        {
            var result = teams.Read()
                .Where(team => team.Members.All(member => DateTime.Now.Year - member.BirthDay.Year >= 10))
                .Select(team => new TeamInfo
                {
                    id = team.Id,
                    name = team.Name,
                    members = team.Members.OrderByDescending(member => member.RegisteredAt).ToList()
                });
            return result.ToList();
        }

        public List<(User, List<Task>)> GetUsersWithTasks()
        {
            var result = new List<(User, List<Task>)>();
            var taskPerformersJoin = tasks.Read().Join(
                users.Read(),
                task => task.PerformerId,
                user => user.Id,
                (task, user) => new
                {
                    task,
                    user
                });
            foreach (var item in taskPerformersJoin)
            {
                item.user.Tasks ??= new List<Domain.Entities.Task>();
                item.user.Tasks.Add(item.task);
            }
            foreach (User user in taskPerformersJoin.Select(item => item.user))
            {
                result.Add((user, user.Tasks.OrderByDescending(task => task.Name).ToList()));
            }
            result = result.OrderBy(item => item.Item1.FirstName).ToList();
            return result;
        }

        public struct UserInfo
        {
            public User user;
            public int numberOfTasksOnTheLastProject;
            public int numberOfUnfinishedOrCancelledTasks;
            public Task longestTask;
        }


    } 
}
