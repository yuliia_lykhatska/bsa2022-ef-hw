﻿using Application.DTO;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application
{
    public class AutomapperProfiles : Profile
    {
        public AutomapperProfiles()
        {
            CreateMap<User, UserDTO>();
            CreateMap<Task, TaskDTO>();
            CreateMap<Team, TeamDTO>();
            CreateMap<Project, ProjectDTO>();
            CreateMap<UserDTO, User>();
            CreateMap<TaskDTO, Task>();
            CreateMap<TeamDTO, Team>();
            CreateMap<ProjectDTO, Project>();
        }
    }
}
