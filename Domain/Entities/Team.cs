﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Entities
{
    public class Team
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set;}
        [Required]
        public DateTime CreatedAt { get; set; } 
        public List<User> Members { get; set; }
        public List<Project> Projects { get; set; }
        public bool IsActive { get; set; }
    }
}
