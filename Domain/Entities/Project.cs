﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Entities
{
    public class Project
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public User Author { get; set; }
        [Required]
        public int TeamId { get; set; }
        public Team Team { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
