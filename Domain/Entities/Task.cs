﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Entities
{
    public class Task
    {
        public int Id { get; set; }
        [Required]
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public int PerformerId { get; set; }
        public User Performer { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(2000)]
        public string Description { get; set; }
        [Required]
        public int State { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
    }
}
